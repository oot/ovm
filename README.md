OVM is a virtual machine platform for programming language implementation. It is distinguished by a small, easy-to-port implementation, yet with the ability to preserve some high-level constructs when used as a target IL.

Ovm is a prototype and is incomplete and buggy. There have not been any releases yet.

Ovm is open-source (Apache 2.0 license).


# Motivation

Ovm is being (very slowly) developed for the Oot programming language (which is also under development), to serve as:
* the language in which Oot will be implemented in
* a target IL (intermediate language) for Oot programs

Ovm is motivated by Oot's aspirations to be an easy-to-port 'glue language' that can run under, interoperate with, and transpile to/from a variety of languages and platforms.

The reason for Ovm's existence is that many other virtual machines:
* are too big: many VMs have large implementations and long specs, making them hard to port
* constrain control flow: many VMs and ILs are written to support a particular HLL (high-level language) and only support control flow patterns of that HLL (for example, some VMs/ILs have trouble with tail calls)
* are too low-level: on the other hand, there are some tiny VMs that consist of only a handful of assembly instructions and few or no registers. When HLL constructs are compiled into assembly, program structure can be difficult to recover; such a language would not be suitable as an IL for transpilation between HLLs

Ovm deals with these issues:
* Ovm has a small implementation. Much of its functionality is in its standard library, which is itself written in Ovm (so you don't have to port it)
* Ovm is assembly-like and supports arbitrary control flow patterns via computed GOTO and exposure of the callstack
* Ovm is slightly higher-level than assembly and provides enough registers for a simple mapping of HLL variables to registers in most cases, but more importantly, it is extensible, which allows the standard library to define some higher-level constructs

The implementation of even relatively simple and common operations as standard library functions makes them inefficient. The motivation of this is just to allow us to have a small core, for portability, and push most of the functionality into the standard library. An Ovm implementation which values efficiency over portability could simply override some of these standard library calls with native implementations. Ovm is structured to make this easy to do.

# What's here

This repo contains:
* ovm: command-line frontend to pymovm
* pymovm: a minimal Ovm implementation (in Python)
* oasm: Ovm assembler
* odis: Ovm disassembler
* pyooblib: a Python library for working with .oob files (and .ob0 files and raw ovm bytecode files)
* cinstr.ob0 and syscall.ob0, part of the 'standard library' of Ovm
* docs: There are no docs yet. For now, see http://www.bayleshanks.com/proj-oot-ootAssemblyThoughts (and possibly the later items in the http://www.bayleshanks.com/proj-oot-ootAssemblyNotes* series eg http://www.bayleshanks.com/proj-oot-ootAssemblyNotes13 ).

Ovm understands three file types (raw and .ob0 formats are understood by the core OVM implementation, .oob format is supported by standard library functions):
* raw: raw Ovm bytecode
* .ob0: a simple module format containing a list of functions, their type signatures, their bytecode implementations
* .oob: "Oot bytecode", a more elaborate module format also containing imports, constant tables, signatures, optional debugging information, etc (not implemented yet).



# Overview of the Ovm language

Ovm is a assembly-like 3-operand CISC memory-to-memory register machine with 2 primitive types, 4 primitive address modes, 16 primitive instructions, 256 registers, a data stack, and a call stack.

Primitive types:
* U16: unsigned 16-bit integer
* PTR: pointer

Primitive address modes:
* immediate
* register
* register indirect (the equivalent of LOAD/STORE)
* stack (post-increment/pre-decrement upon read/write; reading/writing to a stack pointer pops/pushes the stack)

Primitive instructions:
* annotate    (not a real instruction, but an annotation of this point in the code)
* loadi       (load immediate)
* cpy         (copy)
* add-u16-u16 (addition on integers)
* sub-u16-u16 (subtraction on integers)
* leq-u16-u16 (<= comparision on integers)
* add-ptr-u16 (add an integer to a pointer)
* sub-ptr-u16 (subtract an integer from a pointer)
* call3       (call the function represented by a function pointer, passing in 2 inputs and getting 1 output)
* lea         (load effective address: given an addressing mode and operand data, compute an effective address)
* syscall     (call various language or system services)
* jzrel	      (conditional relative branch; branch if input is zero)
* jnzrel      (conditional relative branch; branch if input is non-zero)
* j	      (absolute jump (or static, or immediate jump); jump to a fixed label)
* ji	      (indirect jump (or dynamic jump); jump to a pointer in memory)
* (maybe: swap?)

Ovm will provide compile-time type checking (not yet implemented).


Ovm provides the following features which are atypical for assembly-like languages:
* Generic and ad-hoc polymorphism
* Single-instruction function calls to first-class-functions: a memory location holding a reference to function is called, passing in 2 inputs, and assigning one output
* Capability-based security (not yet implemented)
* Separate data and call stacks; also, stack, and the register file, are first-class and may be swapped in a single instruction to allow concise 'caller saving' of registers, greenthread context switching, etc.
* Extensibility features:
  * custom instructions, written in Ovm
  * custom syscalls, written in Ovm
  * user-defined data structures with getters and setters written in Ovm (not yet implemented)
  * language hooks which call Ovm code (not yet implemented)

Oot has three bytecode encodings, SHORT (16-bit fixed length instructions; defined but not implemented yet), MEDIUM (64-bit fixed length instructions), and LONG (under development; variable-length instructions composed of a 64-bit aligned stream of 16-bit words).

