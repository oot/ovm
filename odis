#!/usr/bin/python3

import argparse, struct, sys
from pyooblib import *
from pyooblib.asmdis import *


####################
# ARGS
####################

parser = argparse.ArgumentParser(description='Disassemble Oot Bytecode')
parser.add_argument('infile', metavar='infile.oob', type=argparse.FileType('rb'), nargs='?',
                    help='bytecode filepath', default='-')
parser.add_argument('outfile', metavar='outfile.txt', type=argparse.FileType('w'), nargs='?',
                    help='destination filepath', default='-')
object_format_group = parser.add_mutually_exclusive_group()
object_format_group.add_argument('--raw', action='store_const', dest='object_format', const='raw', default='oob', help='accept raw bytecode')
object_format_group.add_argument('--ob0', action='store_const', dest='object_format', const='ob0', default='oob', help='accept 0b0 file')
object_format_group.add_argument('--oob', action='store_const', dest='object_format', const='oob', default='oob', help='accept .oob file (default)')
parser.add_argument('--allow_unknown_opcodes', '--anyop', action='store_true', help="allow unknown opcodes")
parser.add_argument('--verbose', '-v', action='count')
parser.add_argument('--version', action='version', version='0.0.0')




#########################
# MAIN
#########################

args = parser.parse_args()


infile, outfile = args.infile, args.outfile


# even though we have argparse.FileType('rb'), if sys.stdin is opened, it's in text mode, and we need binary.
if infile == sys.stdin:
    infile = infile.buffer


if args.object_format=='raw':
    disassemble_raw_bytecode_file(infile,outfile, dis_opts=dis_defs._replace(allow_unknown_opcodes=args.allow_unknown_opcodes))
elif args.object_format=='ob0':
    disassemble_ob0_file(infile,outfile, dis_opts=dis_defs._replace(allow_unknown_opcodes=args.allow_unknown_opcodes, filepath_for_error_messages=infile.name))
elif args.object_format=='oob':
    disassemble_oob_file(infile,outfile, dis_opts=dis_defs._replace(allow_unknown_opcodes=args.allow_unknown_opcodes))




