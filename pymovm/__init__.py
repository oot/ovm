"""
Minimal OVM implementation in Python ('minimal' just means in contrast a larger, more efficient implementation)

Copyright 2016 Bayle Shanks, released under the Apache License, version 2.0.
"""

# todo:
# command-line debug and verbose options to control printing of execution trace
# trap: at some point i gotta go through all the code under run() in OVM and make it so that all the exceptions, instead of being Python exceptions, trap inside the VM; this is so that eg code dynamically executing other code in a sandbox can install a trap handler and get an error message, instead of the VM (or sub-VM) just crashing.
# specify the type signature of primitive instructions and syscalls
# verify() (verifier, type checking, stack maps)
# write basic syscalls like OPEN
# boxed addr mode mode
# define custom addr subspaces with ctor, dtor, getter, setter, len, boxing, serialize, unserialize, strserialize
# capability manipulation
# capability checking for read, write, syscalls (i think i already check execute capability on jump)
# automated tests
# docs (until then, see http://www.bayleshanks.com/proj-oot-ootAssemblyThoughts (and possibly the later items in the http://www.bayleshanks.com/proj-oot-ootAssemblyNotes* series eg http://www.bayleshanks.com/proj-oot-ootAssemblyNotes13 )
# LOADK
# other vm hooks
# my way of doing first-class function returns illegally exposes implementation details (extra call stack entries), fix this
# memory leak when new registers are defined and then finished with, yet still referenced in addr_subspaces
#   this is actually a little tricky since in theory Oob code could capture the registers and try to use them later. How to restrict that? Or do we just use refcounting or tracing GC? TODO
# mallocreg to make it easy for new user code to malloc new registers. Different from mallocraw in that we automatically cpy datastack and codestack to the new register memory.

# to test:
# printf 'sub-u16-u16 $3 6 1\ncpy $4 11\nswap $3 $4\ncpy $5 $3\ncpy stack($0) 7\nsyscall $11 log $5\njnzrel 0 127 252\nsyscall $11 halt 0\n' | ./oasm --raw | ./ovm --raw

# printf 'cpy $3 $6\nadd-ptr-u16 $3 $3 12\nsyscall 0 log $3\nji 0 $3\nsyscall halt\n' | ./oasm --raw | ./ovm --raw --entry_point=0

# the following should be an error, either a datastack underflow or a read past the end of memory:
#   printf 'cpy stack($0) 5\nsyscall 0 log stack($0)\nsyscall 0 log *$0\nsyscall 0 halt\n' | ./oasm --raw | ./ovm --raw --entry_point=0

# test custom instructions
# (printf '.func 100 in:0 0 out:0\ncpy $3 stack($2)\nsyscall 0 log $3\n.endfunc 100\n' | ./oasm --ob0 > /tmp/cinstr_test.ob0); printf '(100) $10 5 7\nsyscall 0 halt\n' | ./oasm --raw | ./ovm --custom_instructions_file /tmp/cinstr_test.ob0 --raw

# test custom syscalls
# (printf '.func 100 in:0 out:0\ncpy $3 stack($0)\nsyscall 0 log $3\ncpy stack($0) 100\nsyscall 0 halt 11\n.endfunc 100\n' | ./oasm --ob0 > /tmp/syscall_test.ob0); printf 'syscall $10 100 5\nsyscall 0 log $3\nsyscall 0 log $10\nsyscall 0 log stack($0)\nsyscall halt\n' | ./oasm --raw | ./ovm --syscalls_file /tmp/syscall_test.ob0 --raw

# test polymorphic dispatch via typearg
# printf 'sub<3> $3 6 1\nsyscall 0 log $3\nsyscall 0 halt 0\n' | ./oasm --raw | ./ovm --raw

# the following should give an unknown opcode error
#   printf 'sub $3 6 1\nsyscall 0 log $3\nsyscall 0 halt 0\n' | ./oasm --raw | ./ovm --raw

# the following should give an error about no suitable implementation for polymorphic opcode found:
#   printf 'sub<1> $3 6 1\nsyscall 0 log $3\nsyscall 0 halt 0\n' | ./oasm --raw | ./ovm --raw

# the following should give an error about a custom instruction failing to preserve the hidden stack:
#   (printf '.func 129 in:0 0 out:0\nsyscall 0 log $3\n.endfunc 129\n' | ./oasm --ob0 > /tmp/cinstr_test.ob0); printf 'sub $3 5 1\nsyscall 0 log $3\nsyscall 0 halt\n' | ./oasm --raw | ./ovm --custom_instructions_file /tmp/cinstr_test.ob0 --raw

# test polymorphic fallthrough dispatch on typearg DYN:
# (printf '.func 129 in:0 0 out:0\ncpy $3 stack($2)\nsyscall 0 log $3\n.endfunc 129\n' | ./oasm --ob0 > /tmp/cinstr_test.ob0); printf 'sub $3 5 1\nsyscall 0 log $3\nsyscall 0 halt\n' | ./oasm --raw | ./ovm --custom_instructions_file /tmp/cinstr_test.ob0 --raw

# test polymorphic dynamic dispatch via typearg given in register
# (printf '.func 129 in:0 0 out:0\ncpy $3 stack($2)\n.endfunc 129\n' | ./oasm --ob0 > /tmp/cinstr_test.ob0); printf 'cpy $4 3\nsub<$4> $3 5 1\nsyscall 0 log $3\ncpy $4 0\nsub<$4> $3 5 1\nsyscall 0 log $3\nsyscall 0 halt\n' | ./oasm --raw | ./ovm --custom_instructions_file /tmp/cinstr_test.ob0 --raw

# sort of 'manual' first-class functions. Note that i did some nasty stuff with locally-unreachable code in custom syscall 2 here.
# (printf '.func 2 in:0 out:0\ncpy 0 stack($0)\ncpy $3 $6\nadd-ptr-u16 $3 $3 8\nsyscall 0 halt $3\nadd-u16-u16 stack($0) stack($0) stack($0)\nji 0 stack($1)\n.endfunc 2\n' | ./oasm --ob0 > /tmp/syscall_test.ob0); printf 'syscall $3 2 0\nsyscall 0 log $3\ncpy stack($0) 10\ncpy stack($0) 20\ncpy stack($1) $6\nadd-ptr-u16 *$1 *$1 8\nji 0 $3\nsyscall 0 halt stack($0)\n' | ./oasm --raw | ./ovm --syscalls_file /tmp/syscall_test.ob0 --raw

# test first-class functions
# (printf '.func 2 in:0 out:0\ncpy 0 stack($0)\ncpy $3 $6\nadd-ptr-u16 $3 $3 8\nsyscall 0 halt $3\nadd-u16-u16 stack($0) stack($0) stack($0)\nji 0 stack($1)\n.endfunc 2\n' | ./oasm --ob0 > /tmp/syscall_test.ob0); printf 'syscall $3 2 0\nsyscall 0 log $3\n($3) $4 10 20\nsyscall 0 halt $4\n' | ./oasm --raw | ./ovm --syscalls_file /tmp/syscall_test.ob0 --raw

# test first-class function via CALL3
# (printf '.func 2 in:0 out:0\ncpy 0 stack($0)\ncpy $3 $6\nadd-ptr-u16 $3 $3 8\nsyscall 0 halt $3\nadd-u16-u16 stack($0) stack($0) stack($0)\nji 0 stack($1)\n.endfunc 2\n' | ./oasm --ob0 > /tmp/syscall_test.ob0); printf 'syscall $3 2 0\nsyscall 0 log $3\ncall3 $4 10 20\nsyscall 0 halt $4\n' | ./oasm --raw | ./ovm --syscalls_file /tmp/syscall_test.ob0 --raw


# test register swap via REGREG
# (printf '.func 2 in:0 out:0\ncpy 0 stack($0)\ncpy $3 $6\nadd-ptr-u16 $3 $3 8\nsyscall 0 halt $3\nadd-u16-u16 stack($0) stack($0) stack($0)\nji 0 stack($1)\n.endfunc 2\n' | ./oasm --ob0 > /tmp/syscall_test.ob0); printf 'syscall $3 2 0\nsyscall 0 log $3\ncall3 $4 10 20\nsyscall 0 halt $4\n' | ./oasm --raw | ./ovm --syscalls_file /tmp/syscall_test.ob0 --raw



from collections import namedtuple
from pyooblib import DATASTACK_REGISTER, CALLSTACK_REGISTER, HIDDENSTACK_REGISTER, PC_REGISTER, REGREG_REGISTER, primitive_opcode_to_mnemonic_hash, primitive_mnemonic_to_opcode_hash, primitive_syscall_opcode_to_signature_hash, primitive_syscall_opcode_to_mnemonic_hash, primitive_syscall_mnemonic_to_opcode_hash, chunk_encoding, decode_SHORT_chunk, decode_MEDIUM_chunk, is_ANNOTATE_instruction, is_LOADI_instruction, opcode_to_signature, ANNOTATE_OPCODE, LOADI_OPCODE, CPY_OPCODE, ADD_U16_U16_OPCODE, SUB_U16_U16_OPCODE, LEQ_U16_U16_OPCODE, ADD_PTR_U16_OPCODE, SUB_PTR_U16_OPCODE, CALL3_OPCODE, LEA_OPCODE, SYSCALL_OPCODE, JZREL_OPCODE, JNZREL_OPCODE, J_OPCODE, JI_OPCODE, SWAP_OPCODE, ADD_OPCODE, SUB_OPCODE, LEQ_OPCODE, opcode_to_nin_nout, InstructionNotFound, HALT_INSTRUCTION_MEDIUM_ENCODED, IMMEDIATE_ADDRESS_MODE, REGISTER_ADDRESS_MODE, REGISTER_INDIRECT_ADDRESS_MODE, STACK_ADDRESS_MODE, SHORT_ENCODING, MEDIUM_ENCODING, opcode_is_polymorphic, PRIMITIVE_POLYMORPHIC_INSTRUCTION_REGISTRATION, VMLOADK_SYSCALL_OPCODE, PRIMITIVE_TYPE_DYN, VMHANDLEDYN_SYSCALL_OPCODE, Oob_short_or_medium_instruction, encode_MEDIUM_instruction



class OvmException(Exception):
    pass
class OvmArgumentCheckingException(OvmException):
    pass
class OvmOutOfBoundsError(OvmException):
    pass
class OvmOutOfBoundsExecutionError(OvmOutOfBoundsError):
    pass
class OvmAlignmentError(OvmException):
    pass
class OvmInternalError(OvmException):
    pass
class OvmEncodingError(OvmException):
    pass
class OvmUnknownInstructionError(OvmEncodingError):
    pass
class OvmInvalidInstructionError(OvmEncodingError):
    pass
class OvmInvalidPointerError(OvmException):
    pass
class OvmCapabilityError(OvmException):
    pass
class OvmExecuteCapabilityError(OvmCapabilityError):
    pass
class OvmTypeError(OvmException):
    # NOTE: an OvmTypeError should never occur in verified code; either the types have been proven to be correct, or dynamic checks have been generated and inserted.
    #       This interpreter uses type tags to distinguish Pointers and UINT16s, but in general an Oob implementation is not required to have type tags at runtime;
    #         in which case, an OvmTypeError would not occur and instead there may even be data corruption.
    pass

class OvmStackUnderflowError(OvmOutOfBoundsError):
    pass
class OvmInsufficientResourceError(OvmException):
    pass
class OvmStackOverflowError(OvmInsufficientResourceError):
    pass
class OvmCustomInstructionDidNotPreserveHiddenStack(OvmException):
    pass




Pointer = namedtuple('Pointer', 'addr_subspace_id local_address')
Context = namedtuple('Context', 'registers registers_addr_subspace_id code_addr_subspace_id PC')
AddressSubspace = namedtuple('AddressSubspace', 'type len_fnptr backing')
# types:
#   'CODE': only these can have EXECUTE capability; 'backing' is a byte array containing Oob code
#   'RAW': 'backing' is an array of items each of which are either U16s, or Pointers
#   other: 'backing' is another AddressSubspace of type 'RAW' (which may hold pointers to further Address_spubspaces of different types)

# all of these addr_subspace accessors are 'privileged': don't call them directly from OOB program code without checking permissions first
class AddressSubspace(object):
    def __init__(self, type, backing, len_fnptr = None):
        self.type = type
        self.backing = backing
        self.len_fnptr = len_fnptr 

    def code(self):
        if self.type != 'CODE':
            raise OvmInternalError('Attempt to access the .code of an address subspace of type other than CODE')
        return self.backing

    def __len__(self):
        if self.type == 'RAW':
            return len(self.backing)
        elif self.type == 'CODE':
            if len(self.backing) % 2 != 0:
                raise OvmInternalError('A CODE addr_subspace %s has size of an odd number of bytes (%d)' % (self, len(a.code)))
            return len(self.backing)//2
        else:
            # call self.len_fnptr and return result
            raise NotImplementedError('Custom address subspaces not implemented yet')
        
    def __repr__(self):
        return "(type={})".format(self.type)

class RawAddressSubspace(AddressSubspace):
    def __init__(self, length):
        AddressSubspace.__init__(self, 'RAW', backing=[0 for x in range(length)])
        # note: in this case the memory is initialized to 0s, but in general OoB implementations are not required to do that

    def __len__(self):
        return len(self.backing)

    def __getitem__(self, key):
        return self.backing[key]

    def __setitem__(self, key, value):
        self.backing[key] = value

    
class CodeAddressSubspace(AddressSubspace):
    """
    note: when code is dynamically loaded, it should inherit syscalls from the code which loaded it, 
          so that if there is a tower of 'virtual' syscall-defined sandboxed code, 
          it can't escape the sandbox and access the primitive syscall implementations. Pass
          the parent's CodeAddressSubspace as 'syscalls'. If a syscall implementation in syscalls
          itself makes a syscall, we will recurse to syscalls.syscalls.
    """
    def __init__(self, code, syscall_addr_subspace_ids={}, len_fnptr = None):
        """
        the 'syscall_addr_subspace_ids' argument is a mapping of syscall opcodes to addr_subspace_ids (and the addr_subspace_ids correspond to CodeAddressSubspaces)
        """
        AddressSubspace.__init__(self, 'CODE', code, len_fnptr = len_fnptr)
        self.type = 'CODE'
        self.backing = code
        self.syscall_addr_subspace_ids = syscall_addr_subspace_ids
        
        self.len_fnptr = len_fnptr

        
 
    
class AddressSubspaces(object):
    def __init__(self):
        self.subspaces = {}
        self.local_capabilities = {}  # key is addr subspace, value is 16-bits of capabilities
        
    def is_pointer_valid(self, addr_subspace_id, local_address):
        """
        SECURITY RISK: THIS LEAKS INFORMATION ABOUT THE LEN OF THE addr_subspace THAT THE POINTER POINTS TO
        """
        if not self.is_addr_subspace_valid(addr_subspace_id):
            return False
        if local_address < 0:
            return False
        if local_address >= len(self.subspaces[addr_subspace_id]):
            return False
        return True

    def is_addr_subspace_valid(self, addr_subspace_id):
        if addr_subspace_id not in self.subspaces:
            return False
        return True


    def __getitem__(self, addr_subspace_id):
        return self.subspaces[addr_subspace_id]

    
    def __setitem__(self, addr_subspace_id, addr_subspace):
        self.subspaces[addr_subspace_id] = addr_subspace
        
        if addr_subspace_id not in self.local_capabilities:
            self.local_capabilities[addr_subspace_id] = 65535

    def __len__(self):
        return len(self.subspaces)
            
    def add(self, new_addr_subspace):
        new_addr_subspace_id = len(self)
        self.subspaces[new_addr_subspace_id] = new_addr_subspace
        self.local_capabilities[new_addr_subspace_id] = 65535
        return new_addr_subspace_id 

class HiddenStack(object):
    def __init__(self, depth=128):
        self._stack = []

    def push(self, x):
        self._stack.append(x)
        
    def pop(self, ):
        try:
            return self._stack.pop()
        except IndexError:
            raise OvmStackUnderflowError('Hidden stack underflow')

    def peek(self, ):
        try:
            return self._stack[-1]
        except IndexError:
            raise OvmStackUnderflowError('Hidden stack underflow')

    def maxdepth(self):
        return 0

    def currentdepth(self):
        return len(self._stack)
    
        
            

CAPABILITY_EXECUTE_BIT = 1
CAPABILITY_WRITE_BIT = 2
CAPABILITY_READ_BIT = 4

# SECURITY NOTE: execute permission implies the ability to query the length of an address subspace


class Vm(object):
    def __init__(self, code_addr_subspace=None, PC=0, code=None, syscalls=None, cinstrs={}, cinstrs_stack_depth=0, datastack_depth=128, callstack_depth=128, hiddenstack_depth=128):
        self.addr_subspaces = AddressSubspaces()

        if (code_addr_subspace is not None) and ((syscalls is not None) or (code is not None)):
            raise OvmArgumentCheckingException("If Vm class is given a 'code_addr_subspace' argument, then it cannot have either a 'code' argument nor a 'syscalls' argument")

        if (syscalls is not None) and (code is None):
            raise OvmArgumentCheckingException("If Vm class is given a 'syscalls' argument, then it must have a 'code' argument")

        if syscalls is not None:
            # wrap provided syscall functions in CodeAddressSpace objects
            syscall_addr_subspace_ids = {}
            for syscall_opcode in syscalls:
                syscall_addr_subspace_ids[syscall_opcode] = self.addr_subspaces.add(CodeAddressSubspace(syscalls[syscall_opcode].code))
                #print ('added syscall addr subspace: %d; %s' % (syscall_addr_subspace_ids[syscall_opcode], syscalls[syscall_opcode]) )

        if code is not None:
            code_addr_subspace=CodeAddressSubspace(code=code, syscall_addr_subspace_ids=syscall_addr_subspace_ids)
            
            

        
        # well-known addr_subspaces ids follow the special registers; 2 = hidden register, 6 = root code. Note that CURRENT code (and current registers, stacks, etc) may vary dynamically. AddressSubspaces may assign new ids by just looking at the number of existing items, so we also have to fill up everything below 6 for this to work.
        
        # save arguments
        # code_addr_space is dealt with below
        self.PC = PC
        
        self.cinstrs = cinstrs
        self.cinstrs_stack_depth=cinstrs_stack_depth


        # initialize lookup data structures
        #self.opcode_to_signature = primitive_opcode_to_signature_hash
        self.opcode_to_mnemonic = dict(primitive_opcode_to_mnemonic_hash)
        self.mnemonic_to_opcode = dict(primitive_mnemonic_to_opcode_hash)

        self.syscall_opcode_to_signature = dict(primitive_syscall_opcode_to_signature_hash)
        self.syscall_opcode_to_mnemonic = dict(primitive_syscall_opcode_to_mnemonic_hash)
        self.syscall_mnemonic_to_opcode = dict(primitive_syscall_mnemonic_to_opcode_hash)

        self.polymorphic_instruction_registration = dict(PRIMITIVE_POLYMORPHIC_INSTRUCTION_REGISTRATION)
        

        # initialize VM internal state
        self.is_halted = False
        self.in_custom_instruction = False
        self.actual_PC = None
        
        self.registers = RawAddressSubspace(256)
        self.hiddenstack = HiddenStack(depth=hiddenstack_depth)
        self.datastack_ptr = Pointer(self.addr_subspaces.add(RawAddressSubspace(datastack_depth)), datastack_depth) # stack is descending and currently empty, so initially the pointer points just past the end of it
        self.callstack_ptr = Pointer(self.addr_subspaces.add(RawAddressSubspace(callstack_depth)), callstack_depth)
        
        self.addr_subspaces.add(self.hiddenstack) # we probably don't need to be register this, since it can't be swapped out, but what the heck, let's register it anyways
        
        #self.registers[0] = Pointer(self.addr_subspaces.add(self.datastack), datastack_depth)  
        #self.registers[1] = Pointer(self.addr_subspaces.add(self.callstack), callstack_depth)
        # register 2 can't be accessed in register mode
        # registers 3,4,5 are GPRs
        # register 6 is PC, which is dynamically calculated
        self.registers[7] = Pointer(self.addr_subspaces.add(self.registers), 0)
        self.register_addr_subspace_id = self.registers[7].addr_subspace_id
        
        if code_addr_subspace is not None:
            self.set_code_addr_subspace_id(self.addr_subspaces.add(code_addr_subspace))

        
        self.syscall_capabilities = set(range(256))

        self.interpreter_recursion_depth = -1 # for debugging
        
        
    def set_code_addr_subspace_id(self, code_addr_subspace_id):
        """
        code should be a CodeAddressSubspace
        """
        
        self.code_addr_subspace_id = code_addr_subspace_id
        #print(self.code_addr_subspace_id)
        self.code_addr_subspace = self.addr_subspaces[self.code_addr_subspace_id]
        #print(self.code_addr_subspace)
        self.code = self.code_addr_subspace.code()
        
        
    def verify(self):
        if self.code is None:
            raise OvmInternalError("Oob code not yet loaded")
        if len(self.code) % 8 != 0:
            raise OvmAlignmentError("Oob code size must be a multiple of 8")
        if len(self.code) == 0:
            raise OvmAlignmentError("Oob code is zero length")


    ################
    # run
    ################
        
    def run(self):
        self.interpreter_recursion_depth += 1
        while not self.is_halted:
            self.run_single_instruction()
        self.interpreter_recursion_depth -= 1
        return self.return_value # set by HALT

    def run_single_instruction(self):
        instruction, instr_length = self.decode_instruction_at(self.PC)
        PC_being_executed = self.PC
        self.PC += instr_length
        self.executeShortOrMediumInstruction(instruction, PC_being_executed)
        
        
    def decode_instruction_at(self, PC):
        if len(self.code) < PC*2 + 8:
            raise OvmOutOfBoundsExecutionError("PC went beyond the end of the code (PC = %d)" % PC)
        
        #print('PC=%s',PC)
        #print(type(PC))
        #print(type(PC // 4))
        #todo i never checked this on SHORT
        #print('chunk from %s to %s',(PC // 4)*8,((PC // 4)+1)*8)
        
        chunk = self.code[(PC // 4)*8:((PC // 4)+1)*8]
        subchunk_PC_index = PC % 4
        _chunk_encoding = chunk_encoding(chunk)
        if (subchunk_PC_index != 0) and (_chunk_encoding != 0):
            # PC % 8 should only be non-zero if we're inside a SHORT instruction
            raise OvmAlignmentError("PC is inside of a non-SHORT chunk (PC = %d)" % PC)

        if _chunk_encoding == 0:
            instructions = decode_SHORT_chunk(chunk)
            return instructions[subchunk_PC_index], 1
        elif _chunk_encoding == 1:
            return decode_MEDIUM_chunk(chunk), 4
        else:
            raise NotImplementedError('LONG encoding not implemented yet (PC = %d)' % PC)
        



    ################
    # resolve operands and execute a SHORT or MEDIUM instruction
    ################
        

    # todo prettify this
    def executeShortOrMediumInstruction(self, instruction, PC_for_error_messages):
        encoding, implementation_dependent_bits, field0_addr_mode, field0_data, field1_addr_mode, field1_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data = instruction

        #print (instruction)
        
        ################
        # handle the 2 instructions with special-case signatures
        ################
        
        if is_ANNOTATE_instruction(instruction):
            return


        if is_LOADI_instruction(instruction):
            if (field3_addr_mode != 0) or (field4_addr_mode != 0):
                raise OvmEncodingError("LOADI has non-immediate addressing mode on field3 or field4 (PC = %d): %s", (PC_for_error_messages, instruction))
                #todo is that a good idea?
            if encoding == 0:
                field3_factor = 4
            elif encoding == 1:
                field3_factor = 256
            else:
                raise NotImplementedError('LONG encoding not implemented yet (PC = %d)' % PC_for_error_messages)
                    
            constant = field3_data*field3_factor + field4_data
            self.resolve_operand_write(field2_addr_mode, field2_data, constant, encoding, PC_for_error_messages=PC_for_error_messages)
            return
            

        ################
        # resolve the typearg operand
        ################

        if encoding == 0:
            typearg = 0  # none of the 16 opcodes available in SHORT are polymorphic
        if encoding == 1:
            typearg = self.resolve_operand_read(field0_addr_mode, field0_data, encoding, PC_for_error_messages=PC_for_error_messages)
        else:
            raise NotImplementedError('LONG encoding not implemented yet (PC = %d)' % PC_for_error_messages)
            


        ################
        # determine the signature, then read the input operands according to the signature
        ################
        
        sig = opcode_to_signature(field1_data)  # note: the signature is computed from the opcode data field BEFORE resolution;
                                                         # this means that if it's a first-class function, then the register referenced
                                                         # controls the signature
        
            
        if sig == ('in', 'imm2'):
            if (field3_addr_mode != 0) or (field4_addr_mode != 0):
                raise OvmEncodingError("Instruction with signature 'in imm2' has non-immediate addressing mode on field3 or field4 (PC = %d): %s", (PC_for_error_messages, instruction))
            if encoding == 0:
                field3_factor = 4
            elif encoding == 1:
                field3_factor = 256
            else:
                raise NotImplementedError('LONG encoding not implemented yet (PC = %d)' % PC_for_error_messages)

            inputs = [self.resolve_operand_read(field2_addr_mode, field2_data, encoding, PC_for_error_messages=PC_for_error_messages), field3_data*field3_factor + field4_data -field3_factor**2//2]
            out_operands = []
        elif sig == ('in', 'in', 'in'):
            in2 = self.resolve_operand_read(field2_addr_mode, field2_data, encoding, PC_for_error_messages=PC_for_error_messages)
            in3 = self.resolve_operand_read(field3_addr_mode, field3_data, encoding, PC_for_error_messages=PC_for_error_messages)
            in4 = self.resolve_operand_read(field4_addr_mode, field4_data, encoding, PC_for_error_messages=PC_for_error_messages)
            inputs = [in2, in3, in4]
            out_operands = []
        elif sig == ('out', 'in', 'in'):
            in3 = self.resolve_operand_read(field3_addr_mode, field3_data, encoding, PC_for_error_messages=PC_for_error_messages)
            in4 = self.resolve_operand_read(field4_addr_mode, field4_data, encoding, PC_for_error_messages=PC_for_error_messages)
            inputs  = [in3, in4]
        elif sig == ('inout', 'inout', 'inout'):
            in2 = self.resolve_operand_read(field2_addr_mode, field2_data, encoding, PC_for_error_messages=PC_for_error_messages)
            in3 = self.resolve_operand_read(field3_addr_mode, field3_data, encoding, PC_for_error_messages=PC_for_error_messages)
            in4 = self.resolve_operand_read(field4_addr_mode, field4_data, encoding, PC_for_error_messages=PC_for_error_messages)
            
            inputs = [in2, in3, in4]



        ################
        # evaluate the instruction
        ################

            
        if field1_addr_mode == 0:
            # this is an ordinary instruction
            opcode = field1_data

            if opcode_is_polymorphic(opcode):
                if typearg != PRIMITIVE_TYPE_DYN:
                    # if typearg == PRIMITIVE_TYPE_DYN then just call the instruction implementation as usual;
                    # implementation of polymorphic instructions in cinstr can define their own method of dynamic dispatch
                    # (eg assume the data operands are tuples (vtable pointer, instance data pointer)?
                    #                   eg assume the data operands are callable pointers and call one of them, passing the other?
                    #                    ...etc)
                    
                    if (opcode, typearg) not in self.polymorphic_instruction_registration:
                        # todo: or should we go to the default implementation of the instruction?
                        # todo: or should we call a VM hook? (or, i guess a general trap-and-resume could have the same effect)
                        raise OvmTypeError('PC=%d: Cannot find an instruction that implements type %d for polymorphic opcode %d' % (PC_for_error_messages, typearg, opcode))

                    opcode = self.polymorphic_instruction_registration[opcode, typearg]


            if opcode == CALL3_OPCODE:
                first_class_function = self.resolve_operand_read(REGISTER_ADDRESS_MODE, 3, encoding, PC_for_error_messages=PC_for_error_messages)
                outputs_go_into = ((field2_addr_mode, field2_data),)
                return self.evaluate_first_class_function(first_class_function, typearg, [inputs[0], inputs[1]], outputs_go_into, PC_for_error_messages=PC_for_error_messages)

                    
            #print("PC=%d opcode=%d" % (PC_for_error_messages, opcode,))
            #print("PC=%d opcode=%d instruction=%s" % (PC_for_error_messages, opcode, instruction))
            output_condition, outputs = self.evaluate_instruction(opcode, typearg, inputs, PC_for_error_messages=PC_for_error_messages)

            ################
            # write the outputs according to the signature, if output_condition
            ################

            if output_condition:
                if sig == ('out', 'in', 'in'):
                    self.resolve_operand_write(field2_addr_mode, field2_data, outputs[0], encoding, PC_for_error_messages=PC_for_error_messages)
                elif sig == ('inout', 'inout', 'inout'):
                    self.resolve_operand_write(field4_addr_mode, field4_data, outputs[2], encoding, PC_for_error_messages=PC_for_error_messages)
                    self.resolve_operand_write(field3_addr_mode, field3_data, outputs[1], encoding, PC_for_error_messages=PC_for_error_messages)
                    self.resolve_operand_write(field2_addr_mode, field2_data, outputs[0], encoding, PC_for_error_messages=PC_for_error_messages)

        else:
            # this is a first-class-function call instruction
            first_class_function = self.resolve_operand_read(field1_addr_mode, field1_data, encoding, PC_for_error_messages=PC_for_error_messages)
            print("PC=%d function=%s" % (PC_for_error_messages, first_class_function))
            

            if sig == ('out', 'in', 'in'):
                outputs_go_into = ((field2_addr_mode, field2_data),)

            elif sig == ('inout', 'inout', 'inout'):
                outputs_go_into = ((field4_addr_mode, field4_data),(field3_addr_mode, field3_data),(field2_addr_mode, field2_data))

            self.evaluate_first_class_function(first_class_function, typearg, inputs, outputs_go_into, PC_for_error_messages=PC_for_error_messages)
                
    ################
    # resolve read/writes to operands
    ################

    # todo prettify this. Much of the ugliness is just error checking, that can be split out.
    def resolve_operand_read(self, addr_mode, data, encoding, PC_for_error_messages):
        if addr_mode == 0: # immediate mode
            return data
        
        elif addr_mode == 1: # register mode
            if encoding == 0 and data <= 1:
                data += 4
            if data == HIDDENSTACK_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Register addressing mode forbidden on register 2 (hidden stack)' % (PC_for_error_messages))
            return self.register_read(data)
        
        elif addr_mode == 2: # register indirect mode (LOAD)
            if data == HIDDENSTACK_REGISTER:
                return self.hiddenstack.peek()
            if data == PC_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Indirect reads forbidden thru register 6 (PC)' % (PC_for_error_messages))
            if data == REGREG_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Indirect reads forbidden thru register 7 (REGREG)' % (PC_for_error_messages))
            else:
                pointer = self.register_read(data)
                if type(pointer) != Pointer:
                    raise OvmTypeError('PC=%d:Addr mode 2 on register %d expected pointer but got: %s' % (data, pointer, PC_for_error_messages))
                if pointer.local_address < 0:
                    raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to read from negative address (while reading addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, data, pointer))
                if pointer.local_address >= len(self.addr_subspaces[pointer.addr_subspace_id]):
                    if data == DATASTACK_REGISTER:
                        raise OvmStackOverflowError('PC=%d:Datastack underflow (while reading in addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, data, pointer))
                    if data == CALLSTACK_REGISTER:
                        raise OvmStackOverflowError('PC=%d:Callstack underflow (while reading in addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, data, pointer))
                    else:
                        raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to read past the end of memory (while reading in addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, data, pointer))
                    
                return self.addr_subspaces[pointer.addr_subspace_id][pointer.local_address]

        elif addr_mode == 3: # stack read mode: register indirect + postincrement (the register) after write 
            if data == HIDDENSTACK_REGISTER:
                return self.hiddenstack.pop()
            if data == PC_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Addr mode 3 reads forbidden thru register 6 (PC)' % (PC_for_error_messages))
            if data == REGREG_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Addr mode 3 reads forbidden thru register 7 (REGREG)' % (PC_for_error_messages))
            else:
                pointer = self.register_read(data)
                if type(pointer) != Pointer:
                    raise OvmTypeError('PC=%d:Addr mode 3 on register %d expected pointer but got: %s' % (data, pointer, PC_for_error_messages))
                if pointer.local_address < 0:
                    raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to read from negative address (while reading addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, data, pointer))
                if pointer.local_address >= len(self.addr_subspaces[pointer.addr_subspace_id]):
                    if data == DATASTACK_REGISTER:
                        raise OvmStackOverflowError('PC=%d:Datastack underflow (while reading in addr mode 3)' % (PC_for_error_messages))
                    if data == CALLSTACK_REGISTER:
                        raise OvmStackOverflowError('PC=%d:Callstack underflow (while reading in addr mode 3)' % (PC_for_error_messages))
                    else:
                        raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to read past the end of memory (while reading addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, data, pointer))
                
                result = self.addr_subspaces[pointer.addr_subspace_id][pointer.local_address]
                self.register_write(data, Pointer(pointer.addr_subspace_id, pointer.local_address+1))
                return result
            
            
            
            
        else:
            raise NotImplementedError('Addressing mode %d not implemented yet' % addr_mode)
        
        
    def resolve_operand_write(self, addr_mode, data, value, encoding, PC_for_error_messages):
        
        if addr_mode == 0: # immediate mode
            # writes to immediate operands are discarded
            return
        
        if data == PC_REGISTER:
            raise OvmInvalidInstructionError('PC=%d:Writes forbidden on or thru register 6 (PC)' % PC_for_error_messages)
        
        if addr_mode == 1: # register mode
            if encoding == 0 and data <= 1:
                data += 4
                
            if data == HIDDENSTACK_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Register addressing mode forbidden on register 2 (hidden stack)' % PC_for_error_messages)
            elif data == DATASTACK_REGISTER:
                if type(data) != Pointer:
                    raise OvmTypeError('PC=%d:Attempt to write non-pointer to datastack register: %s' % (data, PC_for_error_messages))
            elif data == CALLSTACK_REGISTER:
                if type(data) != Pointer:
                    raise OvmTypeError('PC=%d:Attempt to write non-pointer to callstack register: %s' % (data, PC_for_error_messages))
            elif data == REGREG_REGISTER:
                if type(value) != Pointer:
                    raise OvmTypeError('PC=%d: Write to RegReg register (%d) expected pointer but got: %s' % (PC_for_error_messages, data, pointer, ))
                
                self.registers_addr_subspace_id = value.addr_subspace_id
                self.registers = self.addr_subspaces[value.addr_subspace_id]

            
            else:
                self.register_write(data, value)
            return
        
        elif addr_mode == 2: # register indirect mode (STORE)
            if data == HIDDENSTACK_REGISTER:
                self.hiddenstack.pop()
                self.hiddenstack.push(value)
            elif data == REGREG_REGISTER:
                raise OvmInvalidInstructionError('PC=%d: Register indirect address mode write forbidden on register 7 (REGREG)' % PC_for_error_messages)
            else:
                pointer = self.register_read(data)
                if type(pointer) != Pointer:
                    raise OvmTypeError('PC=%d:Addr mode 2 on register %d expected pointer but got: %s' % (data, pointer, PC_for_error_messages))
                if pointer.local_address < 0:
                    raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to write to from negative address (while writing %s in addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, value, data, pointer))
                if pointer.local_address >= len(self.addr_subspaces[pointer.addr_subspace_id]):
                    if data == DATASTACK_REGISTER:
                        raise OvmStackUnderflowError('PC=%d:Datastack underflow (while writing %s in addr mode 3)' % (PC_for_error_messages, value))
                    elif data == CALLSTACK_REGISTER:
                        raise OvmStackUnderflowError('PC=%d:Callstack underflow (while writing %s in addr mode 3)' % (PC_for_error_messages, value))
                    else:
                        raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to write past the end of memory (while writing %s in addr mode 2 thru register %d which contains %s)' % (PC_for_error_messages, value, data, pointer))
                
                self.addr_subspaces[pointer.addr_subspace_id][pointer.local_address] = value
                return
                
        elif addr_mode == 3: # stack write mode: register indirect + predecrement (the register) before write 
            if data == HIDDENSTACK_REGISTER:
                return self.hiddenstack.push(value)
            elif data == PC_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Addr mode 3 writes forbidden thru register 6 (PC)' % PC_for_error_messages)
            elif data == REGREG_REGISTER:
                raise OvmInvalidInstructionError('PC=%d:Stack address mode write forbidden thru register 7 (REGREG)' % PC_for_error_messages)
            else:
                pointer = self.register_read(data)
                if type(pointer) != Pointer:
                    raise OvmTypeError('PC=%d:Addr mode 3 on register %d expected pointer but got: %s' % (data, pointer, PC_for_error_messages))

                if pointer.local_address-1 < 0:
                    if data == DATASTACK_REGISTER:
                        raise OvmStackOverflowError('PC=%d:Datastack overflow (while writing %s in addr mode 3)' % (PC_for_error_messages, value))
                    elif data == CALLSTACK_REGISTER:
                        raise OvmStackOverflowError('PC=%d:Callstack overflow (while writing %s in addr mode 3)' % (PC_for_error_messages, value))
                    else:
                        raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to write to negative address (while writing %s in addr mode 3 thru register %d which contains %s)' % (PC_for_error_messages, value, data, pointer))
                elif pointer.local_address-1 >= len(self.addr_subspaces[pointer.addr_subspace_id]):
                    raise OvmOutOfBoundsExecutionError('PC=%d:Attempt to write past the end of memory (while writing %s in addr mode 3 thru register %d which contains %s)' % (PC_for_error_messages, value, data, pointer))
                    
                else:
                    # todo; also check is local_address is too high (> len of the relevant addr subspace)
                    self.register_write(data, Pointer(pointer.addr_subspace_id, pointer.local_address-1))
                    self.addr_subspaces[pointer.addr_subspace_id][pointer.local_address-1] = value
                    return

                
        else:
            raise NotImplementedError('Addressing modes above 1 not implemented yet')


    # should this be used for something? it's unused and i forgot what i was going to use it for
    def resolve_operand_effective_address(self, addr_mode, data):
        if addr_mode < 2:
            raise OvmInternalError("resolve_operand_effective_address requires an addressing mode >= 2")
        if addr_mode == 0:
            pass
        if addr_mode == 1:
            pass
        else:
            raise NotImplementedError('Addressing modes above 1 not implemented yet')


    """
    * 0: DATASTACK pointer
    * 1: CALLSTACK pointer
    * 2: reserved for implementation-dependent use (also, 'hidden stack' pointer when used from within custom instruction implementation). Register-mode access is forbidden.
    * 3: GPR, but also, in SHORT mode, the only register from which first-class functions can be called.
    * 4: ERR.  In SHORT mode, register addressing mode on 0 is mapped to this.
    * 5: GPR. In SHORT mode, register addressing mode on 1 is mapped to this.
    * 6: PC (read-only) (this points to the NEXT instruction to be executed, not the currently-executing one)
    * 7: REGREG, 'register register', like the stack pointer but for registers.
    """

    #we should probably combine these with the resolve_ methods above
        
    def register_read(self, registerNumber):
        if registerNumber == DATASTACK_REGISTER: # we handle this specially b/c even if we save and restore registers, the datastack and callstack pointers remain unchanged. Manually copying them over is okay, but was difficult to do in our generated first-class-function epilog, so this is better.
            return self.datastack_ptr
        if registerNumber == CALLSTACK_REGISTER:
            return self.callstack_ptr
        elif registerNumber == 6:
            if self.in_custom_instruction:
                #todo 'actual_PC' is dumb, because custom instructions can be inlined, then we should just have one PC
                return Pointer(self.code_addr_subspace_id, self.actual_PC)
            else:
                return Pointer(self.code_addr_subspace_id, self.PC)
        else:
            return self.registers[registerNumber]

    def register_write(self, registerNumber, value):
        if registerNumber == DATASTACK_REGISTER:
            self.datastack_ptr = value
        if registerNumber == CALLSTACK_REGISTER:
            self.callstack_ptr = value
        elif registerNumber == 6:
            raise OvmInvalidInstructionError('attempt to write to PC register')
        else:
            self.registers[registerNumber] = value
    
    

                
    ################
    # the actual instruction execution
    ################
        
        
    def evaluate_instruction(self, opcode, typearg, inputs, PC_for_error_messages):
        #todo since we got rid of conditional instructions, we can get rid of the True/False in the return values
        
        # ANNOTATE and LOADI were taken care of already
        print("%sPC=%d %s<%s> %s" % (' ' * self.interpreter_recursion_depth, PC_for_error_messages, primitive_opcode_to_mnemonic_hash.get(opcode, opcode), typearg, inputs))

        ###### signature: out in in
        if opcode == CPY_OPCODE:
            # inputs[1] unused
            return True, [inputs[0]]

        elif opcode == ADD_U16_U16_OPCODE:
            result = inputs[0] + inputs[1]
            if result > 65535:
                raise OverflowError('PC %d: ADD-U16 result must be <= 65535, but result was %d' % (PC_for_error_messages, result))
            return True, [result]

        elif opcode == SUB_U16_U16_OPCODE:
            result = inputs[0] - inputs[1]
            if result < 0:
                raise OverflowError('PC %d: SUB-U16 result must be > 0, but result was %d' % (PC_for_error_messages, result))
            return True, [result]

        elif opcode == LEQ_U16_U16_OPCODE:
            # todo: dynamic typecheck? what if one of the inputs is a pointer?
            if inputs[0] <= inputs[1]:
                return True, [1]
            else:
                return False, [1]

        elif opcode == ADD_PTR_U16_OPCODE:
            if type(inputs[0]) != Pointer:
                raise OvmTypeError('PC=%d: ADD_PTR_U16 first argument expected pointer but got: %s' % (PC_for_error_messages, inputs[0]))
            addr_subspace_id, local_address = inputs[0]
            new_local_address = local_address + inputs[1]
            # dont check for overflow until the pointer is dereferenced
            return True, [Pointer(addr_subspace_id, new_local_address)]
            

        elif opcode == SUB_PTR_U16_OPCODE:
            if type(inputs[0]) != Pointer:
                raise OvmTypeError('PC=%d: SUB-PTR-U16 expected pointer but got: %s' % (PC_for_error_messages, inputs[0]))
            addr_subspace_id, local_address = inputs[0]
            new_local_address = local_address - inputs[1]
            if new_local_address < 0:
                # todo is this a good idea? it's asymmetric with the behavior of 'ADD'; but we dont want to have to make the implementation hold negative numbers
                raise OverflowError('PC %d: SUB-PTR_U16 local address result must be > 0, but result was %d' % (PC_for_error_messages, new_local_address))
            return True, [Pointer(addr_subspace_id, new_local_address)]
           
        
        elif opcode == LEA_OPCODE:
            addr_mode = inputs[0]
            data = inputs[1]
            if addr_mode < 2:
                raise OvmInvalidInstructionError('LEA requires an address mode >= 2; here it was %d' % addr_mode)
            result = self.resolve_operand_effective_address(addr_mode, data)
            return True, [result]

        elif opcode == SYSCALL_OPCODE:
            return self.do_syscall(inputs[0], inputs[1], self.code_addr_subspace.syscall_addr_subspace_ids, PC_for_error_messages=PC_for_error_messages)
        
        ###### signature: in imm2
        elif opcode == JZREL_OPCODE:
            if inputs[0] == 0:
                if inputs[1] >= 0:
                    relative_offset = inputs[1] + 1
                else:
                    relative_offset = inputs[1]
                self.PC += relative_offset
                return False, []
            else:
                return False, []
        
        elif opcode == JNZREL_OPCODE:
            if inputs[0] != 0:
                if inputs[1] >= 0:
                    relative_offset = inputs[1] + 1
                else:
                    relative_offset = inputs[1]
                self.PC += relative_offset
                return False, []
            else:
                return False, []
        
        elif opcode == J_OPCODE:
            # todo untested
            # inputs[0] is 0 for 'leave registers alone' or a pointer whose address subspace will replace the current ones
            # inputs[1], the immediate argument, is an index into the constant table. At this index we should find a pointer (todo the verifier should check that). Then we jump to that pointer.
            if type(inputs[0]) != Pointer:
                raise OvmInvalidPointerError('PC=%d: J first argument expected pointer but got: %s' % (PC_for_error_messages, inputs[0]))
            if inputs[0] != 0:
                self.registers = self.addr_subspaces[inputs[0].addr_subspace_id]
            constant_index = inputs[1]
            #constant_index_least_significant_byte = constant_index & 255
            #constant_index_most_significant_byte = constant_index >> 8
            ## todo doesnt LOADK have its 64k argument broken into two 8-bit parts? makes me reconsider encoding-dependent imm2
            #   no, it doesn't; now syscalls have one input and one output
            jump_target_pointer = self.do_syscall(self.VMLOADK_SYSCALL_OPCODE, inputs[1], self.code_addr_subspace.syscall_addr_subspace_ids, PC_for_error_messages=PC_for_error_messages)
            self.jump_to(jump_target_pointer, PC_for_error_messages)
            return False, []

        ###### signature: in in in
        
        elif opcode == JI_OPCODE:
            # inputs[2] unused
            # todo the register swapping is untested
            if inputs[0] != 0:
                self.registers = self.addr_subspaces[inputs[0].addr_subspace_id]
            if type(inputs[1]) != Pointer:
                raise OvmInvalidPointerError('PC=%d: JI second argument expected pointer but got: %s' % (PC_for_error_messages, inputs[1]))
            self.jump_to(inputs[1], PC_for_error_messages)
            return False, []
                
        ###### signature: inout inout inout
            
        elif opcode == SWAP_OPCODE:
            # inputs[2] unused
            return True, [inputs[1], inputs[0], inputs[2]]

        ###### signature: depends (on opcode)

        
        elif opcode in self.cinstrs:
            # note that the number of inputs and outputs expected by the custom instruction implementation is assumed to match the signature as determined by the opcode
            # todo the verifier should check this
            # we check it here but implementations are not required to
            opcode_nin_nout = opcode_to_nin_nout(opcode)
            cinstr_nin_nout = len(self.cinstrs[opcode].input_types), len(self.cinstrs[opcode].output_types)
            if opcode_nin_nout != cinstr_nin_nout:
                raise OvmTypeError("PC:%d: Custom instruction opcode-implied signature mismatches its implementation's signature: opcode input count %d, opcode output count %d, but implementation input count %d, implementation input count %d" % (PC_for_error_messages, opcode_nin_nout[0], opcode_nin_nout[1], cinstr_nin_nout[0], cinstr_nin_nout[1]))
        

            return True, self.do_custom_instruction(self.cinstrs[opcode], inputs, PC_for_error_messages)
        
        else:
            # not a hardcoded instruction; todo see if it's a custom instruction
            raise InstructionNotFound("Unknown opcode %d (PC=%d)" % (opcode, PC_for_error_messages))

                
    ################
    # helper functions for evaluate_instruction
    ################

    def do_custom_instruction(self, cinstr, inputs, PC_for_error_messages):
        self.old_hiddenstack_depth = self.hiddenstack.currentdepth()
        for i in range(len(cinstr.input_types)):
            self.hiddenstack.push(inputs[::-1][i])

        self.in_custom_instruction = True
        self.actual_code = self.code
        self.actual_PC = self.PC
        #print('saving PC: %d', self.actual_PC )
        self.code = cinstr.code + HALT_INSTRUCTION_MEDIUM_ENCODED
        # note: at this point code_addr_subspace_id and code_addr_subspace are slightly inconsistent with
        #       what is actually happening, but that's okay because they are only being used to lookup stuff like
        #       syscalls and to get the PC
        
        self.PC = 0
        
        self.run()
        self.is_halted=False
        #print('here')
        
        self.code = self.actual_code
        self.PC = self.actual_PC
        self.in_custom_instruction = False
            
        outputs = []
        for i in range(len(cinstr.output_types)):
            outputs.append(self.hiddenstack.pop())
        outputs = outputs[::-1]

        if self.hiddenstack.currentdepth() != self.old_hiddenstack_depth:
            raise OvmCustomInstructionDidNotPreserveHiddenStack("%sPC %d: Custom instruction failed to preserve hidden stack" % (' ' * self.interpreter_recursion_depth, PC_for_error_messages))
        
        return outputs
        
        
    def jump_to(self, pointer, PC_for_error_messages):
        addr_subspace_id, local_address = pointer
        self.check_addr_subspace(addr_subspace_id, PC_for_error_messages)
        if not self.canjump(addr_subspace_id):
            raise OvmExecuteCapabilityError('PC=%d: Execute access denied for pointer %s' % pointer)
        self.check_pointer(addr_subspace_id, local_address, PC_for_error_messages)
        # should we use self.setCodeAddrSubspace here instead?
        self.set_code_addr_subspace_id(addr_subspace_id)
        self.PC = local_address
   
        
    def evaluate_first_class_function(self, first_class_function, typearg, inputs, outputs_go_into, PC_for_error_messages):
        """
        Unlike ordinary calls, here we use the OVM implementation's call stack. An implementation that doesn't like this could compile the first-class function to multiple instructions (in theory this could blow the inline expansion budget but that's unlikely in practice, anyways you could always add in some chained relative jumps).
        """


        output_placement_instructions = [Oob_short_or_medium_instruction(1, 0, 0, 0, 0, CPY_OPCODE, out_addr_mode, out_data, STACK_ADDRESS_MODE, DATASTACK_REGISTER, 0, 0) for (out_addr_mode, out_data) in outputs_go_into]


        jumpback_instructions = [
                # todo implement REGREG-driven register swap
                Oob_short_or_medium_instruction(1, 0, 0, 0, 0, CPY_OPCODE, REGISTER_ADDRESS_MODE, REGREG_REGISTER, STACK_ADDRESS_MODE, CALLSTACK_REGISTER, 0, 0),
                Oob_short_or_medium_instruction(1, 0, 0, 0, 0, JI_OPCODE, 0, 0, STACK_ADDRESS_MODE, CALLSTACK_REGISTER, 0, 0),
                ]

        epilog = output_placement_instructions + jumpback_instructions
        encoded_epilog = bytearray()
        for instr in epilog:
            encoded_epilog.extend(encode_MEDIUM_instruction(instr))
        epilog_addr_subspace_id = self.addr_subspaces.add(CodeAddressSubspace(bytes(encoded_epilog)))
        
        self.resolve_operand_write(STACK_ADDRESS_MODE, CALLSTACK_REGISTER, Pointer(self.code_addr_subspace_id, self.PC), MEDIUM_ENCODING, PC_for_error_messages)
        self.resolve_operand_write(STACK_ADDRESS_MODE, CALLSTACK_REGISTER, Pointer(self.registers_addr_subspace_id, 0), MEDIUM_ENCODING, PC_for_error_messages)  # or could eval a single instruction that uses regreg to do this
        self.resolve_operand_write(STACK_ADDRESS_MODE, CALLSTACK_REGISTER, Pointer(epilog_addr_subspace_id, 0), MEDIUM_ENCODING, PC_for_error_messages)

        # this dynamic allocation of epilog is real nasty. But if we compiled Oob then we could just put this 'epilog' in the caller, right after the CALL instruction. Also, an
        # optimized interpreter could just put the epilog right on the callstack and have a special sentinel callstack item that lets it know to execute it.

        #todo actually we really need some sort of a sentinel because we can't have these implementation details being visible to the Oob-visible callstack. So we have to pack them together into some sort of magic value.
        
        for input in inputs[::-1]:
            self.resolve_operand_write(STACK_ADDRESS_MODE, DATASTACK_REGISTER, input, MEDIUM_ENCODING, PC_for_error_messages)

        self.set_code_addr_subspace_id(first_class_function.addr_subspace_id)
        self.PC = first_class_function.local_address
        


        
    def do_syscall(self, syscall_opcode, arg, syscall_addr_subspace_ids, PC_for_error_messages):
        # todo check capabilities
        print("%sPC=%d SYSCALL %s %s" % (' ' * self.interpreter_recursion_depth, PC_for_error_messages, primitive_syscall_opcode_to_mnemonic_hash.get(syscall_opcode, syscall_opcode), arg))
        if syscall_opcode in syscall_addr_subspace_ids:
            return True, [self.do_custom_syscall(syscall_addr_subspace_ids[syscall_opcode], arg, PC_for_error_messages)]
        
        if syscall_opcode == self.syscall_mnemonic_to_opcode['halt']:
            self.is_halted = True
            self.return_value = arg
            return True, [0]
        
        if syscall_opcode == self.syscall_mnemonic_to_opcode['log']:
            print(arg)
            return True, [0]
        
        else:
            raise InstructionNotFound("PC=%d: Unknown syscall %d" % (PC_for_error_messages, syscall_opcode))


    def do_custom_syscall(self, syscall_code_subaddr_space_id, arg, PC_for_error_messages):
        """
        Unlike ordinary calls, here we use the OVM implementation's call stack.
        """
        #print('do_custom_syscall passed syscall_code_subaddr_space_id %s' % syscall_code_subaddr_space_id)

        # note that the number of inputs and outputs expected by the custom syscall implementation is assumed to match the signature 'out in'
        # todo the verifier should check this
        # we check it here but implementations are not required to
        #  the following doesnt work yet anyways b/c we didnt save the signatures after reading them in
        #nin_nout = len(self.cinstrs[opcode].input_types), len(self.cinstrs[opcode].output_types)
        #if nin_nout != (1,1):
        #    raise OvmTypeError("PC:%d: Syscall implementation's signature should have 1 input argument and 1 output argument but has %d inputs and %d outputs" % (nin_nout[0], nin_nout[1]))
 

        saved_context = self.save_and_change_context(syscall_code_subaddr_space_id, 0)
        
        self.resolve_operand_write(STACK_ADDRESS_MODE, DATASTACK_REGISTER, arg, MEDIUM_ENCODING, PC_for_error_messages)
        result = self.run()
        self.is_halted = False                   # because the syscall exited via a SYSCALL HALT
        
        self.restore_context(saved_context)
        return result

    def save_and_change_context(self, new_code_subaddr_space_id, pc):
        saved_context = Context(self.registers, self.registers[7].addr_subspace_id, self.code_addr_subspace_id, self.PC)
        
        self.registers = self.new_registers_for_function(self.addr_subspaces[new_code_subaddr_space_id])
        self.registers_addr_subspace_id = self.addr_subspaces.add(self.registers)
        self.set_code_addr_subspace_id(new_code_subaddr_space_id)
        #print('len(self.code): %d' % len(self.code))
        self.PC = pc
        return saved_context

    def restore_context(self, context):

        #context.registers[DATASTACK_REGISTER] = self.registers[DATASTACK_REGISTER]
        #context.registers[CALLSTACK_REGISTER] = self.registers[CALLSTACK_REGISTER]
        
        self.registers = context.registers
        self.addr_subspaces[self.registers_addr_subspace_id]  = 0 # todo memory leak! the entry in the addr_subspaces hash is not released (because that would mess with our naive 'next unique key = len' strategy)
        self.registers_addr_subspace_id = context.registers_addr_subspace_id
        self.set_code_addr_subspace_id(context.code_addr_subspace_id)
        self.PC = context.PC
        

        
        
    
    
    
    def new_registers_for_function(self, f):
        """
        todo, we really only have to make as many registers as this function needs, which could be declared in its attached attributes. But for now
        Someday, at least in most cases, this function could also allocate the new registers on a stack, rather than making a completely new AddressSubspace on the heap.
        """
        new_registers = RawAddressSubspace(256)
        #new_registers[DATASTACK_REGISTER] = self.registers[DATASTACK_REGISTER]
        #new_registers[CALLSTACK_REGISTER] = self.registers[CALLSTACK_REGISTER]
        return new_registers
        
    def canjump(self, addr_subspace_id):
        """
        We assume that we are passed a valid addr_subspace
        """
        addr_subspace_capabilities = self.addr_subspaces.local_capabilities[addr_subspace_id]
        if addr_subspace_capabilities & CAPABILITY_EXECUTE_BIT:
            return True
        return False


    def check_pointer(self, addr_subspace_id, local_address, PC_for_error_messages):
        """
        SECURITY RISK: THIS LEAKS INFORMATION ABOUT THE LEN OF THE addr_subspace THAT THE POINTER POINTS TO
        """
        if not self.addr_subspaces.is_pointer_valid(addr_subspace_id, local_address):
            raise OvmInvalidPointerError('PC=%d: invalid pointer %s %s' % (PC_for_error_messages, addr_subspace_id, local_address))


    def check_addr_subspace(self, addr_subspace_id, PC_for_error_messages):
        if not self.addr_subspaces.is_addr_subspace_valid(addr_subspace_id):
            raise OvmInvalidPointerError('PC=%d: invalid addr_subspace %s' % (PC_for_error_messages, addr_subspace_id))

