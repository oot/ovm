##########
# pyOobLib
#   Library for working with Oot Bytecode
#
#   see http://www.bayleshanks.com/proj-oot-ootAssemblyThoughts

"""
In this file we place code that is shared between different OOB tools, for example, if both an OVM implementation and an OVM assembler/disassembler can make use of something, we put it here rather than duplicating the code.

Stuff that is only needed for assembly/disassembly is in the asmdis.py file in this directory.

Copyright 2016 Bayle Shanks, released under the Apache License, version 2.0.

"""


class OobLibException(Exception):
    pass


class InternalError(OobLibException):
    pass


class InstructionNotFound(OobLibException):
    pass

class NotAOb0FileError(OobLibException):
    pass

class VersionUnsupportedError(OobLibException):
    pass

from collections import namedtuple
import re, sys, struct

OOB_MAGIC_NUMBER = 1953459995 # the four bytes at the beginning of any .oob file should always be this (1B 6F 6F 74; ESC o o t)
OB0_MAGIC_NUMBER = 811757339  # the four bytes at the beginning of any .ob0 file should always be this (1B 6F 62 30; ESC o b 0)
LOWEST_VERSION_UNDERSTOOD = 0
HIGHEST_VERSION_UNDERSTOOD = 0


#########################
# OPCODE MNEMONICS
#########################

"""
here's what the signatures mean:
* imm2: two operands are combined to make a longer immediate. In MEDIUM, this allows the user to specify a 16-bit immediate; in SHORT, a 4-bit immediate. The addr mode bits must be set to mode 0 (immediate mode) in user code, but may be used by the implementation in an implementation-dependent way.
* in: an input
* out: an output
* inout: both an input and an output

as seen in the table below, disregarding the in/out distinction, this means that there are 2 instruction formats allowed (excluding ANNOTATE, but that's a special case):
* x imm2
* x x x

including the in/out distinction, we have (excluding ANNOTATE):
* in imm2
* in in in
* out in in
* inout inout inout

Custom instruction must have one of these 4 signatures.

"""


ANNOTATE_OPCODE = 0
LOADI_OPCODE = 1
CPY_OPCODE = 2
ADD_U16_U16_OPCODE = 3
SUB_U16_U16_OPCODE = 4
LEQ_U16_U16_OPCODE = 5
ADD_PTR_U16_OPCODE = 6
SUB_PTR_U16_OPCODE = 7
CALL3_OPCODE = 8
LEA_OPCODE = 9
SYSCALL_OPCODE = 10
JZREL_OPCODE = 11
JNZREL_OPCODE = 12
J_OPCODE = 13
JI_OPCODE = 14
SWAP_OPCODE = 15

ADD_OPCODE = 128
SUB_OPCODE = 129
LEQ_OPCODE = 130


PRIMITIVE_OPCODES = [
    (ANNOTATE_OPCODE, 'annotate', ()),
    (LOADI_OPCODE, 'loadi', ('out', 'imm2')),
    (CPY_OPCODE, 'cpy', ('out', 'in', 'in')),  # CPY 0 0 is NOP. Third argument is ignored. Note that, if both arguments indirect addr mode or stack addr mode, then this can be a memory-to-memory CPY.
    (ADD_U16_U16_OPCODE, 'add-u16-u16', ('out', 'in', 'in')),  # U16 + U16
    (SUB_U16_U16_OPCODE, 'sub-u16-u16', ('out', 'in', 'in')),  # U16 - U16
    (LEQ_U16_U16_OPCODE, 'leq-u16-u16', ('out', 'in', 'in')),  # U16 <= U16
    (ADD_PTR_U16_OPCODE, 'add-ptr-u16', ('out', 'in', 'in')),  # PTR + U16
    (SUB_PTR_U16_OPCODE, 'sub-ptr-u16', ('out', 'in', 'in')),  # PTR - U16
    (CALL3_OPCODE, 'call3', ('out', 'in', 'in')), # CALL the function held in register 3, passing two inputs and taking one output. This is provided so that SHORT mode can make first-class function calls.
    (LEA_OPCODE, 'lea', ('out', 'in', 'in')),   # input arguments are the addr mode (must not be 0) and data of an operand, output is the effective address
    (SYSCALL_OPCODE, 'syscall', ('out', 'in', 'in')), # argument is syscall number (what are the other two input arguments? inputs to the syscall?
    (JZREL_OPCODE, 'jzrel', ('in', 'imm2')),  # conditional relative jump (branch if 'in' is zero). Note: imm2 input is signed. 1 is added to non-negative inputs (b/c 0 is just an infinite loop, not useful), so range is (-32768:-1 unioned with 1:32768)
    (JNZREL_OPCODE, 'jnzrel', ('in', 'imm2')), # conditional relative jump (branch if 'in' is not zero)
    (J_OPCODE, 'j', ('in', 'imm2')),        # absolute jump (to a pointer in the constant table). The first argument is 0 to leave registers alone, or a pointer to replace the registers with the addr subspace of the pointer; the second argument is the index into the constant table.
    (JI_OPCODE, 'ji', ('in', 'in', 'in')),    # indirect branch. The first argument is as the first argument of j; the second argument is the jump target. Second and third arguments are unused.
    (SWAP_OPCODE, 'swap', ('inout', 'inout', 'inout')), # swap the first and second arguments. Third argument is unused.
    # polymorphic:
    (ADD_OPCODE, 'add', ('out', 'in', 'in')),  # x + y
    (SUB_OPCODE, 'sub', ('out', 'in', 'in')),  # x - y
    (LEQ_OPCODE, 'leq', ('out', 'in', 'in')),  # x1 <= x2
    ]

PRIMITIVE_TYPE_DYN     = 0
PRIMITIVE_TYPE_PTR     = 1
PRIMITIVE_TYPE_U16     = 2
PRIMITIVE_TYPE_U16_U16 = 3
PRIMITIVE_TYPE_PTR_U16 = 4

PRIMITIVE_POLYMORPHIC_INSTRUCTION_REGISTRATION = {
    (ADD_OPCODE, PRIMITIVE_TYPE_U16_U16): ADD_U16_U16_OPCODE,
    (SUB_OPCODE, PRIMITIVE_TYPE_U16_U16): SUB_U16_U16_OPCODE,
    (LEQ_OPCODE, PRIMITIVE_TYPE_U16_U16): LEQ_U16_U16_OPCODE,
    (ADD_OPCODE, PRIMITIVE_TYPE_PTR_U16): ADD_PTR_U16_OPCODE,
    (SUB_OPCODE, PRIMITIVE_TYPE_PTR_U16): SUB_PTR_U16_OPCODE,
    }
    
    
def opcode_to_signature(opcode):
    """
    The 4 least-significant-bits of the opcode (the opcode mod 16) determine the signature of the function. Opcodes 0 and 1 (ANNOTATE and LOADI) are special cases.
    """
    
    opcode_mod_16 = opcode & 15
    if opcode == 0:
        return ()
    if opcode == 1:
        return ('out')
    if opcode_mod_16 <= 10:
        return ('out', 'in', 'in')
    if opcode_mod_16 <= 13:
        return ('in', 'imm2')
    if opcode_mod_16 <= 14:
        return ('in', 'in', 'in')
    if opcode_mod_16 == 15:
        return ('inout', 'inout', 'inout')
    raise InternalError('This should be unreachable')

def opcode_is_polymorphic(opcode):
    return opcode >= 128

def opcode_to_nin_nout(opcode):
    # returns number of input and output arguments

    sig = opcode_to_signature(opcode)
    if sig == ('out'):
        return 0,1
    elif sig == ('out', 'in', 'in'):
        return 2,1
    elif sig == ('in', 'imm2'):
        return 2,0
    elif sig == ('in', 'in', 'in'):
        return 3,0
    elif sig == ('inout', 'inout', 'inout'):
        return 3,3
    else:
        raise InternalError('Unknown signature')


#todo the opcode->mnemonic ones could be arrays for speed
primitive_mnemonic_to_opcode_hash = {}
primitive_mnemonic_to_signature_hash = {}
primitive_opcode_to_mnemonic_hash = {}
primitive_opcode_to_signature_hash = {}
for (opcode, mnemonic, signature) in PRIMITIVE_OPCODES:
    primitive_mnemonic_to_opcode_hash[mnemonic] = opcode
    primitive_mnemonic_to_signature_hash[mnemonic] = signature
    primitive_opcode_to_mnemonic_hash[opcode] = mnemonic
    primitive_opcode_to_signature_hash[opcode] = signature

PRIMITIVE_OPCODE_TABLE = [primitive_mnemonic_to_opcode_hash, primitive_mnemonic_to_signature_hash, primitive_opcode_to_mnemonic_hash, primitive_opcode_to_signature_hash]

#SHORT_OPCODES_TO_PRIMITIVE_OPCODES = [primitive_mnemonic_to_opcode_hash[mnemonic] for mnemonic in ['ANNOTATE', 'JZ', 'JZREL', 'JNZREL', 'CALL', 'CPYZ', 'ADD-U16', 'LEQ-U16', 'JZI', 'SUB-U16', 'SYSCALL', 'CSWAP', 'CALL4', 'LOADI', 'LEA']]


#########################
# ANNOTATION TYPES
#########################
ANNOTATIONTYPE_STACKDEPTH = 5

#########################
# SYSCALLS
#########################


HALT_SYSCALL_OPCODE = 0
LOG_SYSCALL_OPCODE = 1
VMLOADK_SYSCALL_OPCODE = 2


VMHANDLEDYN_SYSCALL_OPCODE = 10

PRIMITIVE_SYSCALLS = [
    (HALT_SYSCALL_OPCODE, 'halt', ()),
    (LOG_SYSCALL_OPCODE, 'log', ()),
    (VMLOADK_SYSCALL_OPCODE, 'vmloadk', ()),
    (VMHANDLEDYN_SYSCALL_OPCODE, 'vmhandledyn', ()),
]

primitive_syscall_mnemonic_to_opcode_hash = {}
primitive_syscall_mnemonic_to_signature_hash = {}
primitive_syscall_opcode_to_mnemonic_hash = {}
primitive_syscall_opcode_to_signature_hash = {}
for (opcode, mnemonic, signature) in PRIMITIVE_SYSCALLS:
    primitive_syscall_mnemonic_to_opcode_hash[mnemonic] = opcode
    primitive_syscall_mnemonic_to_signature_hash[mnemonic] = signature
    primitive_syscall_opcode_to_mnemonic_hash[opcode] = mnemonic
    primitive_syscall_opcode_to_signature_hash[opcode] = signature

#########################
# ADDRESS MODES
#########################
IMMEDIATE_ADDRESS_MODE = 0
REGISTER_ADDRESS_MODE = 1
REGISTER_INDIRECT_ADDRESS_MODE = 2
STACK_ADDRESS_MODE = 3

#########################
# ENCODING
#########################
SHORT_ENCODING = 0
MEDIUM_ENCODING = 1
LONG_ENCODING = 2
# note: in the actual encoded instructions, the encoding field is a prefix code, hence it's 0, 10, 11

#########################
# SPECIAL REGISTERS
#########################
DATASTACK_REGISTER = 0
CALLSTACK_REGISTER = 1
HIDDENSTACK_REGISTER = 2; RESERVED0_REGISTER = 2
FIRSTCLASSFUNCTION_REGISTER = 3
ERR_REGISTER = 4
# no special name for 5, it's just a GPR
PC_REGISTER = 6
REGREG_REGISTER = 7

 
#########################
# ENCODED INSTRUCTION CONSTANTS
#########################

HALT_INSTRUCTION_MEDIUM_ENCODED = bytes((1, 0, 16*SYSCALL_OPCODE, 0, 0, 0, 16*HALT_SYSCALL_OPCODE, 0))
 
#########################
# INSTRUCTION DECODING ROUTINES
#########################

Oob_short_or_medium_instruction = namedtuple('Oob_short_or_medium_instruction', 'encoding implementation_dependent_bits field0_addr_mode field0_data field1_addr_mode field1_data field2_addr_mode field2_data field3_addr_mode field3_data field4_addr_mode field4_data')
# encoding is 0 for SHORT or 1 for MEDIUM. This is used when we have imm2 signature operands, to know how to combine the last 2 fields.

def chunk_encoding(chunk):
    """
    Takes 8 bytes and returns 0 for SHORT, 1 for MEDIUM, 2 for LONG.

    Oot Bytecode is grouped into 64-bit chunks. The first two bits
    (in little-endian ordering) of each chunk tell what format it is in
    (either SHORT, MEDIUM, or LONG). If the first bit is 0, it's SHORT.
    Otherwise, if the first bit is 1 and the second bit is 0, then it's MEDIUM.
    Or, if both of the first two bits are both 1, then it's LONG.
    """
    if not (chunk[0] & 1):
        return 0
    elif not (chunk[0] & 2):
        return 1
    else:
        return 2
    
    
def decode_SHORT_chunk(chunk):
    raise NotImplementedError('SHORT encoding not implemented yet')
    
 

def decode_MEDIUM_chunk(chunk):
    """
    MEDIUM encoding is 64-bits (little-endian, and with the bits in each byte from least-significant to most-significant). From least-significant-bit to most-significant-bit:
    * 2 bits: format (for MEDIUM, this is constant 1 ('10' in little-endian binary))
    * 2 bits: implementation_dependent_bits (an integer in between 0 and 3, inclusive)
    * 5x fields:
    **     4 bits: field addr mode (an integer in between 0 and 15, inclusive)
    **     8 bits: field data (an integer in between 0 and 255, inclusive)
    """

    format_bits = chunk[0] & 3
    
    if (type(chunk) != bytes) or (len(chunk) != 8):
        raise InternalError('decode_MEDIUM_chunk requires a chunk of 8 bytes')
    
    if format_bits != 1:
        raise InternalError('decode_MEDIUM_chunk called on a non-MEDIUM chunk')

    
    implementation_dependent_bits = (chunk[0] >> 2) & 3
    field0_addr_mode = (chunk[0] >> 4)
    field0_data = chunk[1]
    field1_addr_mode = chunk[2] & 15
    field1_data = (chunk[2] >> 4) + 16*(chunk[3] & 15)
    field2_addr_mode = chunk[3] >> 4
    field2_data = chunk[4]
    field3_addr_mode = chunk[5] & 15
    field3_data = (chunk[5] >> 4) + 16*(chunk[6] & 15)
    field4_addr_mode = chunk[6] >> 4
    field4_data = chunk[7]


    
    oob_short_or_medium_instruction = Oob_short_or_medium_instruction(1, implementation_dependent_bits, field0_addr_mode, field0_data, field1_addr_mode, field1_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data)
    
    return oob_short_or_medium_instruction


def decode_LONG_from_stream(infile):
    raise NotImplementedError('LONG encoding not implemented yet')
    # return long_instruction, how_many_read, next_chunk 
    # how_many_read is in units of 16 bits

def test_oob_short_or_medium_instruction_invalid(oob_short_or_medium_instruction):
    encoding, implementation_dependent_bits, typearg_addr_mode, typearg_data, opcode_addr_mode, opcode_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data = oob_short_or_medium_instruction
    
    if is_ANNOTATE_instruction(oob_short_or_medium_instruction):
        if typearg_addr_mode != 0 or typearg_data != 0:
            return True
            
        if field2_addr_mode != 0:
            return True
        annotation_type = field2_data
        if annotation_type == ANNOTATIONTYPE_STACKDEPTH:
            if field3_addr_mode or field4_addr_mode:
                return True
            
    return False


def decode_stack_depth_annotation_instruction(oob_short_or_medium_instruction):
    # assumes that this is actually a stack_depth annotation, that is, that field2_data == ANNOTATIONTYPE_STACKDEPTH
    return oob_short_or_medium_instruction.field4_data + 4*oob_short_or_medium_instruction.field3_data

def is_ANNOTATE_instruction(oob_short_or_medium_instruction):
    if (oob_short_or_medium_instruction.field1_addr_mode == 0) and (oob_short_or_medium_instruction.field1_data == ANNOTATE_OPCODE):
        return True
    else:
        return False

def is_LOADI_instruction(oob_short_or_medium_instruction):
    if (oob_short_or_medium_instruction.field1_addr_mode == 0) and (oob_short_or_medium_instruction.field1_data == LOADI_OPCODE):
        return True
    else:
        return False
    



#########################
# file format subroutines
#########################

Funcdef = namedtuple('Funcdef', 'input_types output_types codeLength code directives')
    
def check_magic_number(infile, magic_number, exception_class=OobLibException, filetype_shortname='', magic_number_additional_text='', filepath_for_error_messages=''):
    # the arguments after magic_number, magic_number_byte_length are just to give nice error messages
    magic_number_bytes = infile.read(4)
    if len(magic_number_bytes) < 4:
        raise(exception_class("Putative %s file %s is less than 4 bytes long, which cannot be valid" % (filetype_shortname, filepath_for_error_messages)))
    file_magic_number = struct.unpack("<I", magic_number_bytes)[0] # little-endian unsigned int (4 bytes)
    if file_magic_number != magic_number:
        raise(exception_class("First 4 bytes ('magic number') of putative %s file '%s' should be %s%s, but was %s" % (filetype_shortname, filepath_for_error_messages, magic_number, magic_number_additional_text, file_magic_number)))


def check_version(bs, min_version, max_version, version_too_lo_exception_class=OobLibException, version_too_hi_exception_class=OobLibException, filetype_shortname='', filepath_for_error_messages=''):
    version = struct.unpack("<H", bs)[0] # little-endian unsigned short (2 bytes)
    if (version < min_version):
        raise(version_too_lo_exception_class("Version unsupported: %s file '%s' is version %s, but only versions %d through %d are supported" % (filetype_shortname, filepath_for_error_messages, version, min_version, max_version)))
    if (version > max_version):
        raise(version_too_hi_exception_class("Version unsupported: %s file '%s' is version %s, but only versions %d through %d are supported" % (filetype_shortname, filepath_for_error_messages, version, min_version, max_version)))
    return version

def get_bytes_or_error(infile, num_bytes, exception_class=OobLibException, filetype_shortname='', filepath_for_error_messages=''):
    if num_bytes < 0:
        raise(InternalError("Attempt to read negative number of bytes"))
    if num_bytes > 0:
        bs = infile.read(num_bytes)
    else:
        bs = bytes()
    if len(bs) < num_bytes:
        raise(exception_class("Putative %s file %s is invalid (unexpected EOF)" % (filetype_shortname, filepath_for_error_messages)))
    return bs

def get_ushort_or_error(infile, **kw):
    bs = get_bytes_or_error(infile, 2, **kw)
    return struct.unpack("<H", bs)[0] # little-endian unsigned short (2 bytes)

def compute_padding_length(length_without_padding, align_to):
    return (align_to - (length_without_padding % align_to)) % align_to

#########################
# OB0 file format
#########################
OB0_FILE_MAGIC_NUMBER_BYTES = (0x1B, 0x6F, 0x62, 0x30) # bytes 0-3 are the magic number: 1B 6F 62 30 (ESC o b 0) (811757339 as a little-endian 32-bit integer)
OB0_FILE_MAGIC_NUMBER_STR = "1B 6F 62 30"

def load_ob0_file(infile, filepath_for_error_messages='', relax_size_limit=False):
    gboekw = {'exception_class':NotAOb0FileError, 'filetype_shortname':'.ob0', 'filepath_for_error_messages':filepath_for_error_messages}
    
    check_magic_number(infile, OB0_MAGIC_NUMBER, NotAOb0FileError, '.ob0', ' (%s)' % OB0_FILE_MAGIC_NUMBER_STR, filepath_for_error_messages)
    bs = get_bytes_or_error(infile, 2, **gboekw)
    version = check_version(bs, 0, 0, VersionUnsupportedError, VersionUnsupportedError, '.ob0', filepath_for_error_messages)
    header_length_without_padding = get_ushort_or_error(infile, **gboekw)
    stack_depth = get_ushort_or_error(infile, **gboekw)

    if stack_depth > 65535 and (not relax_stack_depth_limit):
        raise NotAOb0FileError("Invalid .ob0 file: .stackdepth must be <= 65535")
        # note: this doesn't mean that OVM is limited to a stack of 65535, just that this file format can't express a greater
        #       number in this parameter. You may want to set the stack_depth parameter to 0 in your ob0 file, which in some
        #       contexts may indicate a request for a larger stack depth.

    
    function_count = get_ushort_or_error(infile, **gboekw)

    function_ids_offsets_and_lengths = []
    for _ in range(function_count):
        funcId = get_ushort_or_error(infile, **gboekw)
        if funcId > 65535:
            raise NotAOb0FileError("Invalid .ob0 file: function IDs must be <= 65535")
        offset = get_ushort_or_error(infile, **gboekw)
        codeLength = get_ushort_or_error(infile, **gboekw)
        function_ids_offsets_and_lengths.append((funcId, offset, codeLength))

    currentPos = (4+2+2+2+2) + function_count*(2+2+2)
    if currentPos > header_length_without_padding:
        raise(NotAOb0FileError("Putative %s file %s is invalid (header longer than indicated)" % ('ob0', filepath_for_error_messages)))
        
    header_padding_length = compute_padding_length(header_length_without_padding, 2)
    header_length = header_length_without_padding + header_padding_length
    
    implementation_dependent_bytes = get_bytes_or_error(infile, header_length_without_padding - currentPos, **gboekw)
    _ = get_bytes_or_error(infile, header_padding_length, **gboekw)
    currentPos = header_length

    

    
    functions = {}
    for funcId, offset, codeLength in function_ids_offsets_and_lengths:
        if currentPos > offset:
            raise(NotAOb0FileError("Putative %s file %s is invalid (overlapping sections; current position %d, but next offset %d)" % ('ob0', filepath_for_error_messages, currentPos, offset)))

        #seek to next offset
        _ = get_bytes_or_error(infile, offset - currentPos, **gboekw)
        currentPos = offset
        
        item_header_length = get_ushort_or_error(infile, **gboekw)
        input_count = get_ushort_or_error(infile, **gboekw)
        output_count = get_ushort_or_error(infile, **gboekw)
        currentPos += 6
        if input_count > 255 or output_count > 255:
            raise NotAOb0FileError("Invalid .ob0 file: functions must have <= 255 inputs and <= 255 outputs")
            
        
        input_types = []
        for _ in range(input_count):
            input_types.append(get_ushort_or_error(infile, **gboekw))
        currentPos += 2*input_count
            
        output_types = []
        for _ in range(output_count):
            output_types.append(get_ushort_or_error(infile, **gboekw))
        currentPos += 2*output_count

        #skip padding
        _ = get_bytes_or_error(infile, offset + item_header_length - currentPos, **gboekw)
        currentPos += offset + item_header_length - currentPos
        
        if (currentPos + codeLength > 65535) and (not relax_size_limit):
            raise(NotAOb0FileError("Putative %s file %s is invalid; ob0 files have a length limit of 65535 bytes" % ('ob0', filepath_for_error_messages)))
        
        code = get_bytes_or_error(infile, codeLength, **gboekw)
        currentPos += codeLength

        
        functions[funcId] = Funcdef(input_types, output_types, codeLength, code, ())
            
        
    
    return stack_depth, functions
    #todo
    
    
# included b/c our pymovm dynamically generates MOV and JI instructions in order to place the return value from first-class functions

def encode_MEDIUM_instruction(oob_short_or_medium_instruction):
    """
    returns an 8-byte 'bytes' containing the encoded instruction.

    MEDIUM encoding is 64-bits (little-endian, and with the bits in each byte from least-significant to most-significant). From least-significant-bit to most-significant-bit:
    * 2 bits: format (for MEDIUM, this is constant 1 ('10' in little-endian binary))
    * 2 bits: implementation_dependent_bits (an integer in between 0 and 3, inclusive)
    * 5x fields:
    **     4 bits: field addr mode (an integer in between 0 and 15, inclusive)
    **     8 bits: field data (an integer in between 0 and 255, inclusive)
    """

    encoding, implementation_dependent_bits, field0_addr_mode, field0_data, field1_addr_mode, field1_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data = oob_short_or_medium_instruction
    
    byte0  = 1 + 4*implementation_dependent_bits + 16*field0_addr_mode
    byte1 = field0_data
    byte2  = field1_addr_mode + 16*(field1_data & 15)
    byte3 = (field1_data >> 4) + 16*field2_addr_mode
    byte4 = field2_data
    byte5  = field3_addr_mode + 16*(field3_data & 15)
    byte6 = (field3_data >> 4) + 16*field4_addr_mode
    byte7 = field4_data


    return bytes((byte0, byte1, byte2, byte3, byte4, byte5, byte6, byte7))

