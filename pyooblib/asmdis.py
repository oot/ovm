from collections import namedtuple
from . import *
import io

class ParseError(OobLibException):
    pass

class InvalidCodeError(OobLibException):
    pass

# Copyright 2016 Bayle Shanks, released under the Apache License, version 2.0.


# a test case:
# printf '.stackdepth 33\nANNOTATE 0 11 0 # hi hi\n.func 2 in:2 2 out:1 2\n.stackdepth 201\nLEA 0 10 0\n.endfunc 2' | ./oasm.py --ob0 | ./odis.py --ob0 --anyop

#########################
# ENCODING SUBROUTINES
#########################

def encode_instruction(oob_instruction):
    if isinstance(oob_instruction, Oob_short_or_medium_instruction):
        return encode_MEDIUM_instruction(oob_instruction)
    else:
        raise InternalError("encode_instruction requires a oob_short_or_medium_instruction")
    
## SHORT not implemented yet

## MEDIUM

def make_annotation_instruction(annotation_type, *rest):
    if annotation_type == ANNOTATIONTYPE_STACKDEPTH:
        stack_depth = rest[0]
        stack_depth_first_two_bits = stack_depth & 3
        stack_depth_other_bits = stack_depth >> 2
        # the point of splitting it up is so that annotations of stack depths up to 7 can fit into SHORT
        return Oob_short_or_medium_instruction(field2_data=ANNOTATIONTYPE_STACKDEPTH, field3_data=stack_depth_other_bits,  field4_data=stack_depth_first_two_bits,
                                               implementation_dependent_bits=0, field0_addr_mode=0, field0_data=0, field1_addr_mode=0, field1_data=0, field2_addr_mode=0, field3_addr_mode=0, field4_addr_mode=0, encoding=None)
    else:
        raise InternalError("unknown annotation_type %s" % annotation_type)


#########################
# ASSEMBLY AND DISASSEMBLY SUBROUTINES
#########################
Disassembly_options = namedtuple('Disassembly_options', 'allow_unknown_opcodes opcode_table filepath_for_error_messages annotations_to_directives crash_on_invalid')
dis_defs = Disassembly_options(allow_unknown_opcodes=False, opcode_table=PRIMITIVE_OPCODE_TABLE, filepath_for_error_messages='', annotations_to_directives=True, crash_on_invalid=False)

Assembly_options = namedtuple('Assembly_options', 'opcode_table use_SHORT')
asm_defs = Assembly_options(opcode_table=PRIMITIVE_OPCODE_TABLE, use_SHORT=False)

Funcdefasm = namedtuple('Funcdefasm', 'input_types output_types begin end directives')


Identifier = namedtuple('Identifier', 'type value')

class Assembly_state(object):
    
    def __init__(self, PC=0, directives=None, labels=None, functions=None, infunction=None):
        self.PC = PC
        if labels is None:
           labels = {}
        self.labels = labels
        if functions is None:
            functions = {}
        self.functions = {}
        if directives is None:
           directives = {}
        self.directives = directives
        self.infunction = infunction
        
        self.identifiers = {}
        for syscall_mnemonic in primitive_syscall_mnemonic_to_opcode_hash: # this is kinda hacky; we just create constants with the names of each syscall mnemonic, so you can write eg "SYSCALL LOG 0 0"
            self.identifiers[syscall_mnemonic] = Identifier(type='CONSTANT', value=primitive_syscall_mnemonic_to_opcode_hash[syscall_mnemonic])
            
        
    def __repr__(self):
        return "PC={}; labels={}; functions={}; infunction={}; directives={}".format(self.PC, self.labels, self.functions, self.infunction, self.directives)

    def add_directive(self, directive, values):
        if self.infunction is not None:
            self.functions[self.infunction].directives[directive] = values
        else:
            self.directives[directive] = values
            

    def enter_function(self, funcId, input_types, output_types):
        if self.infunction is not None:
            raise ParseError("Nested .func directives not allowed in Oot Assembly")
        else:
            try:
                funcId = int(funcId)
            except ValueError:
                raise ParseError("Invalid .func id (must be integer): %s" % funcId)

            if funcId > 65535:
                raise ParseError(".func id greater than max of 65535: %d" % funcId)
            
            if funcId in self.functions:
                raise ParseError("Duplicate .func id %d" % funcId)

            self.functions[funcId] = Funcdefasm(input_types, output_types, self.PC, None, {})

            self.infunction = funcId

    def leave_function(self, funcId):
        if self.infunction is None:
            raise ParseError("Non-matching .endfunc")
        if self.infunction != funcId:
            raise ParseError("Non-matching .endfunc (in function %d but .endfunc %d)" % (self.infunction, funcId))
        self.functions[self.infunction] = self.functions[self.infunction]._replace(end=self.PC)
        self.infunction = None
        
    def create_label(self, label):
        self.labels[label] = self.PC
        

def opcode_to_mnemonic(opcode, dis_opts=dis_defs):
    mnemonic_to_opcode_hash, mnemonic_to_signature_hash, opcode_to_mnemonic_hash, opcode_to_signature_hash = dis_opts.opcode_table
    
    try:
        return opcode_to_mnemonic_hash[opcode]
    except KeyError:
        if dis_opts.allow_unknown_opcodes:
            return str(opcode)
        else:
            raise InstructionNotFound("Opcode not found: %s" % opcode)


def mnemonic_to_opcode(mnemonic, asm_opts=asm_defs):
    mnemonic_to_opcode_hash, mnemonic_to_signature_hash, opcode_to_mnemonic_hash, opcode_to_signature_hash = asm_opts.opcode_table
    try:
        return mnemonic_to_opcode_hash[mnemonic]
    except:
        raise InstructionNotFound("Mnemonic not found: %s" % mnemonic)


def disassemble_operand(addr_mode, data, sig=None):
    
    if (addr_mode & 3) == 0:
        result = hex(data)
    elif (addr_mode & 3) == 1:
        result = '$' + str(data)
    elif (addr_mode & 3) == 2:
        result = '*$' + str(data)
    elif (addr_mode & 3) == 3:
        result = 'stack($' + str(data) + ')'

    if addr_mode & 4:
        result = 'box(' + result + ')'

    if addr_mode & 8:
        return 'meta(' + result + ')'
        
    return result

def assemble_expression(expression_text, asm_state):
    m = re.match(r'^-?[0-9]+$', expression_text)
    if m:
        return int(expression_text)
    m = re.match(r'^-?0x[0-9]+$', expression_text)
    if m:
        return int(expression_text, 16)
    
    m = re.match(r'^[a-zA-Z_][a-zA-Z_0-9]*$', expression_text)
    if m:
        return asm_resolve_identifier(expression_text, asm_state=asm_state)

    raise ParseError("Can't parse expression in operand: %s" % expression_text)

def asm_resolve_identifier(identifier, asm_state):
    
    m = re.match(r'^[a-zA-Z_][a-zA-Z_0-9]*$', identifier)
    if not m:
        raise ParseError("Can't parse identifier: %s" % identifier)

    if identifier in asm_state.identifiers:
        id_type, val = asm_state.identifiers[identifier]
        if id_type == 'CONSTANT':
            #print(val, file=sys.stderr)
            return val
    
    raise ParseError("Identifier not found: %s" % identifier)
    
    

def assemble_operand(operand_text, asm_state):
    """
    returns addr_mode, data
    """
    addr_mode = 0
    
    addressed_expression = operand_text
    m = re.match(r'^meta\((.*)\)$', addressed_expression)
    if m:
      addr_mode = addr_mode + 8
      addressed_expression = m.group(1)

    m = re.match(r'^box\((.*)\)$', addressed_expression)
    if m:
      addr_mode = addr_mode + 4
      addressed_expression = m.group(1)

    m = re.match(r'^stack\(\$([\w-]+)\)$', addressed_expression)
    if m:
      data = assemble_expression(m.groups()[0], asm_state=asm_state)
      addr_mode = addr_mode + 3
      return (addr_mode, data)

    m = re.match(r'^\*\$([\w-]+)$', addressed_expression)
    if m:
      data = assemble_expression(m.groups()[0], asm_state=asm_state)
      addr_mode = addr_mode + 2
      return (addr_mode, data)


    m = re.match(r'^\$([\w-]+)$', addressed_expression)
    if m:
      data = assemble_expression(m.groups()[0], asm_state=asm_state)
      addr_mode = addr_mode + 1
      return (addr_mode, data)

    m = re.match(r'^[\w-]+$', addressed_expression)
    if m:
      data = assemble_expression(addressed_expression, asm_state=asm_state)
      addr_mode = addr_mode
      return (addr_mode, data)

    raise ParseError("Can't parse operand: %s" % operand_text)


def assemble_line(line, asm_state, asm_opts=asm_defs):

    # strip trailing whitespace and comments
    m = re.match(r'^(.*?)\s*(#.*)?$', line)
    line = m.groups()[0]

    if line == '':
        return None, asm_state
    
    m = re.match(r'^\.(?P<directive>\w+)\s*(?P<rest>.*)', line)
    if m:
        return assemble_directive_line(m.group('directive'), m.group('rest'), asm_state, asm_opts=asm_defs)

    m = re.match(r'^:([A-Z][A-Z0-9]*)$', line)
    if m:
        return assemble_label_line(m.groups()[0], asm_state, asm_opts=asm_defs)

    return assemble_instruction_line(line, asm_state, asm_opts=asm_defs)


def assemble_lines(lines, asm_opts=asm_defs, asm_state=None):
    if asm_state is None:
        asm_state = Assembly_state()
    
    code = bytearray()
    for line in lines:
        instruction, asm_state = assemble_line(line.rstrip(), asm_state)
        if instruction:
            encoded_instruction = encode_instruction(instruction)
            if len(encoded_instruction) % 2 != 0:
                raise InternalError("Encoded instruction size appears to be an odd number of bytes; it should always be even")
            code.extend(encoded_instruction)
            # note, if len(encoded_instruction) were odd then the following line would cause problems;
            asm_state.PC += int(len(encoded_instruction)/2)
            
    return code, asm_state

def terminate_assemble_lines(asm_state, asm_opts=asm_defs):
    if asm_state.infunction is not None:
        raise ParseError("Source code ended in function %s; .endfunc %s missing" % (asm_state.infunction, asm_state.infunction,))
        


def assemble_directive_line(directive, rest, asm_state, asm_opts=asm_defs):
    if directive == 'stackdepth':
        try:
            stack_depth = int(rest)
        except ValueError:
            raise ParseError(".stackdepth requires an integer but got: %s" % rest)
        
        instruction = make_annotation_instruction(ANNOTATIONTYPE_STACKDEPTH, stack_depth)
            
        asm_state.add_directive(ANNOTATIONTYPE_STACKDEPTH, int(rest))
        
        return instruction, asm_state


    if directive == 'func':
        m = re.match(r'^(\d+) in:([0-9 ]*?) out:([0-9 ]*)$', rest)
        if not m:
            raise ParseError("Can't parse: .func %s" % rest)
        
        try:
            funcId = int(m.groups()[0])
        except ValueError:
            raise ParseError(".func id must be integer but got: %s" % funcId)
            
        input_types_text = m.groups()[1]
        output_types_text = m.groups()[2]

        try:
            input_types = list(map(int, input_types_text.split()))
        except ValueError:
            raise ParseError(".func input_types must be integers")

        try:
            output_types = list(map(int, output_types_text.split()))
        except ValueError:
            raise ParseError(".func output_types must be integers")

        asm_state.enter_function(funcId, input_types, output_types)
        return None, asm_state

    if directive == 'endfunc':
        m = re.match(r'^(\d+)$', rest)
        if not m:
            raise ParseError("Can't parse: .endfunc %s" % rest)
        funcId = int(m.groups()[0])
        asm_state.leave_function(funcId)
        return None, asm_state

    
            
    else:
        raise ParseError("Unknown directive: %s" % directive)
       

def assemble_label_line(label, asm_state, asm_opts=asm_defs):
    asm_state.create_label(label)
    return None, asm_state

def assemble_instruction_line(line, asm_state, asm_opts=asm_defs):
    try:
        return assemble_MEDIUM_line(line, asm_state, asm_opts=asm_opts)
    except ParseError as err:
        raise err

def assemble_opcode_field(opcode_field_text, asm_state, asm_opts=asm_defs):
    # todo support other addr modes here
    m = re.match('^\((.*)\)$', opcode_field_text)
    if m:
        return assemble_operand(m.groups()[0], asm_state)
    m = re.match('^[0-9]+$', opcode_field_text)
    if m:
        return 0, int(opcode_field_text)
    
    return 0, mnemonic_to_opcode(opcode_field_text, asm_opts=asm_opts)
    
def assemble_MEDIUM_line(line, asm_state, asm_opts=asm_defs):
    m = re.match(r'^([^ <]*)(?:<([^ >]*)>)? ?([^<>#]*)(?: +#([0-9]+))?$', line)
    if not m:
        raise ParseError("Can't parse line as MEDIUM: %s" % line)
    opcode_field_text, mb_typearg, rest,  implementation_dependent_bits_text = m.groups()
    opcode_addr_mode, opcode_data = assemble_opcode_field(opcode_field_text, asm_state=asm_state, asm_opts=asm_opts)
    if mb_typearg:
        typearg_addr_mode, typearg_data = assemble_operand(mb_typearg, asm_state)
    else:
        typearg_addr_mode, typearg_data = 0, 0
    if implementation_dependent_bits_text:
        implementation_dependent_bits = int(implementation_dependent_bits_text)
    else:
        implementation_dependent_bits = 0
        
    other_fields = []
    m = re.match(r'^(\S+)(?: (.*))?$', rest)
    while m:
        other_fields.append(assemble_operand(m.groups()[0], asm_state))
        if len(m.groups()) > 1 and m.groups()[1]:
            rest = m.groups()[1]
            m = re.match(r'^(\S+)(?: (.*))?$', rest)
        else:
            rest = ''
            m = None

    if not re.match(r'^\s*$', rest):
        raise ParseError("Can't parse line as MEDIUM: %s" % line)

    
    if len(other_fields) == 0:
        field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data = 0,0,0,0,0,0

    elif len(other_fields) == 1:
        # todo combine these guys so that we can represent numbers bigger than 255
        field2_addr_mode, field2_data = other_fields[0]
        field3_addr_mode, field3_data = 0, 0
        field4_addr_mode, field4_data = 0, 0


    elif len(other_fields) == 2:
        # todo typecheck and combine these guys so that we can represent numbers bigger than 255
        field2_addr_mode, field2_data = other_fields[0]
        field3_addr_mode, field3_data = other_fields[1]
        field4_addr_mode, field4_data = 0, 0


    elif len(other_fields) == 3:
        field2_addr_mode, field2_data = other_fields[0]
        field3_addr_mode, field3_data = other_fields[1]
        field4_addr_mode, field4_data = other_fields[2]

    #print(Oob_short_or_medium_instruction(1, implementation_dependent_bits, typearg_addr_mode, typearg_data, opcode_addr_mode, opcode_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data), file=sys.stderr)
    return Oob_short_or_medium_instruction(1, implementation_dependent_bits, typearg_addr_mode, typearg_data, opcode_addr_mode, opcode_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data), asm_state
    


    
def annotation_to_directive(oob_short_or_medium_instruction):
    encoding, implementation_dependent_bits, field0_addr_mode, field0_data, field1_addr_mode, field1_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data = oob_short_or_medium_instruction

    annotation_type = field2_data
    if annotation_type == ANNOTATIONTYPE_STACKDEPTH:
        stack_depth = decode_stack_depth_annotation_instruction(oob_short_or_medium_instruction)
        return '.stackdepth %d' % stack_depth

    else:
        # unknown annotation
        return ''
    

def disassemble_SHORT_or_MEDIUM_instruction(oob_short_or_medium_instruction, dis_opts=dis_defs):
    encoding, implementation_dependent_bits, field0_addr_mode, field0_data, field1_addr_mode, field1_data, field2_addr_mode, field2_data, field3_addr_mode, field3_data, field4_addr_mode, field4_data = oob_short_or_medium_instruction

    is_invalid = test_oob_short_or_medium_instruction_invalid(oob_short_or_medium_instruction)
    if is_invalid and dis_opts.crash_on_invalid:
        raise InvalidCodeError("{}".format(oob_short_or_medium_instruction))

    if is_invalid:
        invalid_text = '!'
    else:
        invalid_text = ''
        

    
    if implementation_dependent_bits == 0:
        implementation_dependent_text = ''
    else:
        implementation_dependent_text = '  ^%d' % (implementation_dependent_bits)

    if is_ANNOTATE_instruction(oob_short_or_medium_instruction) and dis_opts.annotations_to_directives:
        directive = annotation_to_directive(oob_short_or_medium_instruction)
        if directive:
            return "%s%s%s" % (invalid_text, directive, implementation_dependent_text)
        #else unknown annotation, just go on and print it as ANNOTATE
        
    if (field0_addr_mode == 0) and (field0_data == 0):
        typearg_text = ''
    else:
        typearg_text = '<' + disassemble_operand(field0_addr_mode, field0_data) + '>'

        
    if (field1_addr_mode == 0):
        opcode_text = opcode_to_mnemonic(field1_data, dis_opts=dis_opts)
    else:
        opcode_text = '(' + disassemble_operand(field1_addr_mode, field1_data) + ')'

    field2_text = disassemble_operand(field2_addr_mode, field2_data)
    field3_text = disassemble_operand(field3_addr_mode, field3_data)
    field4_text = disassemble_operand(field4_addr_mode, field4_data)

    return "%s%s%s %s %s %s%s" % (invalid_text, opcode_text, typearg_text, field2_text, field3_text, field4_text, implementation_dependent_text)



def if_annnotation_instruction_then_record_it(instruction, annotations):
    if (instruction.field1_addr_mode == 0) and (instruction.field1_data == 1):
        pass
        # todo: record annotations

    
def disassemble_SHORT_chunk(chunk, pos, annotations, dis_opts=dis_defs):
    raise NotImplementedError('SHORT encoding not implemented yet')
    instructions = decode_SHORT_chunk(chunk)
    result = ''
    for instruction in instructions:
        try:
            result += disassemble_SHORT_or_MEDIUM_instruction(instruction, dis_opts=dis_opts) + '\n'
        except InvalidCodeError as e:
            raise InvalidCodeError("Invalid code at line %d: %s" % (pos, str(e)))
        if_annnotation_instruction_then_record_it(instruction, annotations)
        pos += 1
        
def disassemble_MEDIUM_chunk(chunk, pos, annotations, dis_opts=dis_defs):
    instruction = decode_MEDIUM_chunk(chunk)

    try:
        text = disassemble_SHORT_or_MEDIUM_instruction(instruction, dis_opts=dis_opts)
    except InvalidCodeError as e:
        raise InvalidCodeError("Invalid code at line %d: %s" % (pos, str(e)))

    if_annnotation_instruction_then_record_it(instruction, annotations)
    
    return text + '\n'
    


def disassemble_LONG_from_stream(infile, initial_chunk, pos, annotations):
    raise NotImplementedError('LONG encoding not implemented yet')
    return text, how_many_read, next_chunk # how_many_read is in units of 16 bits



# we keep track of POS so that we can index annotated lines by lineno
def disassemble_raw_bytecode_file(infile,outfile, pos=0, annotations={}, dis_opts=dis_defs):
    chunk = infile.read(8)    # OOB code is in 64-bit groupings
    while chunk:              # note: bytes((0,0,0,0,0,0,0,0)) is truthy
        _chunk_encoding = chunk_encoding(chunk)
        #print(_chunk_encoding, file=sys.stderr)

        if _chunk_encoding == 0:   # SHORT encoding
            outfile.write(disassemble_SHORT_chunk(chunk, pos, annotations, dis_opts=dis_opts))
            chunk = infile.read(8)
            pos += 4 # pos is in units of 16 bits
        elif _chunk_encoding == 1: # MEDIUM encoding
            outfile.write(disassemble_MEDIUM_chunk(chunk, pos, annotations, dis_opts=dis_opts))
            chunk = infile.read(8)
            pos += 4 # pos is in units of 16 bits
        else:                      # LONG encoding
            # since LONG instructions may span multiple chunks, we give control to the
            # disassemble_LONG subroutine which will continue until it hits a non-LONG chunk (or EOF),
            # at which point it will return and pass back the LONG instruction and the next non-LONG chunk.
            text, how_many_read, chunk = disassemble_LONG_from_stream(infile, chunk, pos, annotations)
            pos += how_many_read
            outfile.write(text)    
        

def disassemble_ob0_file(infile, outfile, dis_opts=dis_defs):
    stack_depth, functions = load_ob0_file(infile, filepath_for_error_messages=dis_opts.filepath_for_error_messages)
    print('.stackdepth %d' % stack_depth)
    for funcId in functions:
        f = functions[funcId]
        input_types_text = ' '.join([str(it) for it in f.input_types])
        output_types_text = ' '.join([str(ot) for ot in f.output_types])
        print()
        print('.func %d in:%s out:%s' % (funcId, input_types_text, output_types_text) )
        disassemble_raw_bytecode_file(io.BytesIO(f.code), outfile, dis_opts=dis_opts)
        print('.endfunc %d' % funcId)
        
    #todo

def disassemble_oob_file(infile,outfile):
    raise NotImplementedError('oob disassembly not implemented yet')


def assemble_ob0_file(infile,asm_opts=asm_defs):
    code, asm_state = assemble_lines(infile, asm_opts=asm_opts)
    terminate_assemble_lines(asm_state)
    
    stack_depth = asm_state.directives.get(ANNOTATIONTYPE_STACKDEPTH, 0)

    functions = {}
    for funcId in asm_state.functions:
        funcdefasm = asm_state.functions[funcId]
        funcCode = code[(funcdefasm.begin*2):(funcdefasm.end*2)]
        funcdef = Funcdef(funcdefasm.input_types, funcdefasm.output_types, len(funcCode)/2, funcCode, funcdefasm.directives)
        functions[funcId] = funcdef
    return stack_depth, functions, code, asm_state

#########################
# OB0 file format
#########################

def compute_padding_length(length_without_padding, align_to):
    return (align_to - (length_without_padding % align_to)) % align_to

def save_ob0_file(fh, stack_depth, functions, implementation_dependent_header_bytes=bytes(), relax_size_limit=False, relax_stack_depth_limit=False):
    """
    functions is a hash from integers to Funcdefs, where the id in the Funcdef is an int and the code in the Funcdef is a bytes(). Directives in the Funcdefs are ignored.
    """

    if stack_depth > 65535 and (not relax_stack_depth_limit):
        raise NotAOb0FileError("save_ob0_file asked to generate an .ob0 file with a stack_depth greater than the max of 65535")
        # note: this doesn't mean that OVM is limited to a stack of 65535, just that this file format can't express a greater
        #       number in this parameter. You may want to set the stack_depth parameter to 0 in your ob0 file, which in some
        #       contexts may indicate a request for a larger stack depth.
        
       
    OB0_FILE_VERSION = 0

    for funcId in functions:
        if funcId > 65535:
            raise NotAOb0FileError("save_ob0_file asked to generate an .ob0 file with functions with an ID greater than the max of 65535")
    
    for func in functions.values():
        if len(func.input_types) > 255 or len(func.output_types) > 255:
            raise NotAOb0FileError("save_ob0_file asked to generate an .ob0 file with functions with more than 255 inputs or outputs")
            

    
    # compute lengths of item headers, and padding
    item_header_paddings = []
    item_header_lengths = []
    for func in functions.values():
        item_header_length_without_padding = 2+2+2 + 2*len(func.input_types) + 2*len(func.output_types)
        item_header_padding = compute_padding_length(item_header_length_without_padding, 8)
        item_header_paddings.append(item_header_padding)
        item_header_lengths.append(item_header_length_without_padding + item_header_padding)
    
    # compute offsets for TOC
    item_offsets_from_end_of_header = []
    next_offset_from_end_of_header = 0
    for func, item_header_length in zip(functions.values(), item_header_lengths):
        item_offsets_from_end_of_header.append(next_offset_from_end_of_header)
        next_offset_from_end_of_header += item_header_length + len(func.code)
        if len(func.code)/2 != func.codeLength:
            raise(NotAOb0FileError("save_ob0_file was passed inconsistent codeLengths: %d vs actual length of %d" % (len(func.code), func.codeLength/2)))

    header_length_without_padding = (4+2+2+2+2) + len(functions)*(2+2+2) + len(implementation_dependent_header_bytes)
    header_padding_length = compute_padding_length(header_length_without_padding, 2)
    header_length = header_length_without_padding + header_padding_length

    if (next_offset_from_end_of_header + header_length > 65535) and (not relax_length_limit):
        raise NotAOb0FileError("save_ob0_file asked to generate an .ob0 file longer than the length limit of 65535 bytes")
    
    fh.write(bytes(OB0_FILE_MAGIC_NUMBER_BYTES)) 
    fh.write(struct.pack("<H", OB0_FILE_VERSION)) 
    fh.write(struct.pack("<H", header_length_without_padding)) 
    fh.write(struct.pack("<H", stack_depth)) 
    fh.write(struct.pack("<H", len(functions)))
    for funcId, offset_from_end_of_header in zip(functions, item_offsets_from_end_of_header):
        func = functions[funcId]
        fh.write(struct.pack("<H", funcId))
        fh.write(struct.pack("<H", offset_from_end_of_header + header_length))
        fh.write(struct.pack("<H", len(func.code)))
    fh.write(implementation_dependent_header_bytes)
    fh.write(bytes((0,) * header_padding_length))
    for func, item_header_length, item_header_padding in zip(functions.values(), item_header_lengths, item_header_paddings):
        fh.write(struct.pack("<H", item_header_length))
        fh.write(struct.pack("<H", len(func.input_types)))
        fh.write(struct.pack("<H", len(func.output_types)))
        for input_type in func.input_types:
            fh.write(struct.pack("<H", input_type))
        for output_type in func.output_types:
            fh.write(struct.pack("<H", output_type))
        fh.write(bytes((0,) * item_header_padding))
        fh.write(func.code)
